'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.createTable('direccion', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        pais: {
          type: Sequelize.STRING,
          allowNull: false
        },
        estado: {
          type: Sequelize.STRING,
          allowNull: false
        },
        ciudad: {
          type: Sequelize.STRING,
          allowNull: false
        },
        colonia: {
          type: Sequelize.STRING,
          allowNull: true
        },
        calle: {
          type: Sequelize.STRING,
          allowNull: false
        },
        codigo_postal: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        numero_ext: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        numero_int: {
          type: Sequelize.INTEGER,
        },
        direccion_text: {
          type: Sequelize.STRING,
        },
        lat: {
          type: Sequelize.TEXT
        },
        long:{
          type: Sequelize.TEXT
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      await queryInterface.sequelize.query('CREATE TRIGGER direccion_created_trigger_text'+
      ' BEFORE INSERT ON direccion FOR EACH ROW SET NEW.createdAt = NOW();'),
      await queryInterface.sequelize.query('CREATE TRIGGER direccion_updated_trigger_text'+
      ' BEFORE UPDATE ON direccion FOR EACH ROW SET NEW.updatedAt = NOW();')
    ]
  },
  down: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.sequelize.query('DROP TRIGGER direccion_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER direccion_updated_trigger_text'),
      await queryInterface.dropTable('direccion'),
    ];
  }
};