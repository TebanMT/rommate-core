import { UserRepository } from "./user-repository";
import { CodigoVerificacionRepository } from "./codigo-verificacion-repository";
import { EspacioRepository } from "./espacio-repository";
import { DireccionRepository } from "./direccion-repository";
import { EspacioReglasRepository } from "./espacio-reglas-repository";
import { EspacioSeguridadPrivRepository } from "./espacio-seguridadpriv-repository";
import { EspacioServiciosRepository } from "./espacio-servicios-repository";
import { MultimediaRepository } from "./multimedia-repository";
import { PaymentRepository } from "./payment-repository";
import { MensajeRepository } from "./mensaje-repository";
import { NotificaionesRepository } from "./notificaciones-repository";

export {
    UserRepository,
    CodigoVerificacionRepository,
    EspacioRepository,
    DireccionRepository,
    EspacioReglasRepository,
    EspacioSeguridadPrivRepository,
    EspacioServiciosRepository,
    MultimediaRepository,
    PaymentRepository,
    MensajeRepository,
    NotificaionesRepository
}