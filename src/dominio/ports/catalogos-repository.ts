import {Estado, Municipio, TipoEspacio, Servicios, SeguridadPrivacidad, Reglas} from "../entities/catalogos-entity";

export interface CatalogosRepository {

    getCatalogoActivosEstados(activo?: boolean): Promise<Estado[] | null>;
    getCatalogoActivosMunicipios(idEstado: number, activo?: boolean): Promise<Municipio[] | null>;
    getCatalogoActivosTipoEspacio(activo?: boolean): Promise<TipoEspacio[] | null>;
    getCatalogoActivosServicios(activo?: boolean): Promise<Servicios[] | null>;
    getCatalogoActivosSeguridadPrivacidad(activo?: boolean): Promise<SeguridadPrivacidad[] | null>;
    getCatalogoActivosReglasEspacio(activo?: boolean): Promise<Reglas[] | null>;

}