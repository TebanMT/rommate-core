import { Reglas, SeguridadPrivacidad, Servicios } from "../dominio/entities/catalogos-entity";
import { Direccion } from "../dominio/entities/direccion-entity";
import { Espacio } from "../dominio/entities/espacio-entity";
import { Multimedia } from "../dominio/entities/multimedia-entity";
import { DireccionRepository, EspacioRepository,
         EspacioReglasRepository, EspacioSeguridadPrivRepository,
         EspacioServiciosRepository, MultimediaRepository,
         UserRepository 
        } from "../dominio/ports";

export class EspacioService {

    private espacioRepo     : EspacioRepository;
    private dirRepo?        : DireccionRepository;
    private espRegRepo?     : EspacioReglasRepository;
    private eSegPrivRepo?   : EspacioSeguridadPrivRepository;
    private eServiciosRepo? : EspacioServiciosRepository;
    private userRepo?       : UserRepository;


    constructor(espacioRepository  : EspacioRepository,
                direccionRepo?     : DireccionRepository,
                espacioReglasRepo? : EspacioReglasRepository,
                eSeguridadPrivRepo?: EspacioSeguridadPrivRepository,
                eServiciosRepo?    : EspacioServiciosRepository,
                userRepo?          : UserRepository){
        this.espacioRepo    = espacioRepository;
        this.dirRepo        = direccionRepo;
        this.espRegRepo     = espacioReglasRepo;
        this.eSegPrivRepo   = eSeguridadPrivRepo;
        this.eServiciosRepo = eServiciosRepo;
        this.userRepo       = userRepo;
    }

    async getMinumunDataEspacios(lugar?:string, tipo?:string, fecha?:string, id_anfitrion?:string): Promise<Espacio[] | null>{
        if (id_anfitrion) {
            return this.espacioRepo.getMinumunDataEspaciosByAnfitrion(id_anfitrion);
        }else{
            return this.espacioRepo.getMinumunDataEspacios(lugar, tipo, fecha);
        }
    }

    async createEspacio(espacio: Espacio, dir: Direccion, reglas: Reglas[],
        segPriv: SeguridadPrivacidad[], servicios: Servicios[]): Promise<Espacio>{
            const dirResult      = await this.dirRepo?.createDireccion(dir);
            espacio.id_direccion = dirResult?.id!;
            const espacioResult  = await this.espacioRepo.createEspacio(espacio);
            await this.espRegRepo?.createEspacioReglas(reglas,espacioResult.id);
            await this.eSegPrivRepo?.createEspacioSegPriv(segPriv,espacioResult.id);
            await this.eServiciosRepo?.createEspacioServicios(servicios,espacioResult.id);
            //await this.multimediaRepo?.createMultimedia(multimedia,espacioResult.id);
            await this.userRepo?.updateAnfirion(espacioResult.id_usuario,true);
        return espacioResult;
    }

    async getEspacio(idEspacio: number): Promise<Espacio | null>{
        const result = await this.espacioRepo.getEspacio(idEspacio);
        return result;
    }

    async getCountCities(){
        return await this.espacioRepo.getCountCities();
    }

    async changeActivo(id_espacio:number, status:boolean){
        return await this.espacioRepo.changeActivo(id_espacio,status);
    }

    async deleteEspacio(id_espacio:number):Promise<boolean>{
        return await this.espacioRepo.deleteEspacio(id_espacio);
    }


}
