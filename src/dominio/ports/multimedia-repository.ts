import { Multimedia } from "../entities/multimedia-entity";

export interface MultimediaRepository {
    createMultimedia(multimedia: Multimedia[], id_espacio:number): Promise<boolean>;
}