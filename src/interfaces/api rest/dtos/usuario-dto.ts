export interface UsuarioDTO{
    id: number;
    nombre: string;
    apellidos: string;
    email: string;
    lada_int: string;
    numero_cel: string;
    usuario: boolean;
    anfitrion: boolean;
    url_ID_government: string;
    token?: string;
}