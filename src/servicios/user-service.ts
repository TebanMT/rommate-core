import Usuario from "../dominio/entities/user-entity";
import { UserRepository } from "../dominio/ports";
import { TokenManager } from "./security/tokenManager";

export class UserService {
    private userRepo: UserRepository;
    private token?: TokenManager;

    //simula una sobrecaga del constructor
    constructor(UserRepository: UserRepository, TokenManager?: TokenManager){
        this.userRepo = UserRepository;
        this.token = TokenManager;
    }

    async find(id: number):Promise<Usuario | null>{
        const user = await this.userRepo.find(id);
        return user;
    }

    async registerUser(usuario: Usuario): Promise<Usuario>{
        const token = await this.token?.generate({ tel: usuario.numero_cel });
        const usuarioDomain: Usuario = await this.userRepo.createUser(usuario);
        usuarioDomain.token = token;
        return usuarioDomain;
    }

    async login(usuario: Usuario): Promise<Usuario | null>{
        //Busca el usuario con su numero de telefono
        var usuarioDomain: Usuario;
        const thereIsUser = await this.userRepo.getUserByPhone(usuario.lada_int, usuario.numero_cel);
        if (thereIsUser) {
            usuarioDomain = thereIsUser;
            //Genera un token
            const token = await this.token?.generate({ tel: usuario.numero_cel });
            usuarioDomain.token = token;
        } else {
            return null;
        }
        return usuarioDomain;
    }

    async existUser(usuario: Usuario): Promise<boolean> {
        return await this.userRepo.existUser(usuario);
    }
}