'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.createTable('seguridad_privacidad', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        nombre: {
          type: Sequelize.STRING,
          allowNull: false
        },
        descripcion: {
          type: Sequelize.TEXT
        },
        activo: {
          type: Sequelize.BOOLEAN
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      await queryInterface.sequelize.query('CREATE TRIGGER seguridad_privacidad_created_trigger_text'+
      ' BEFORE INSERT ON seguridad_privacidad FOR EACH ROW SET NEW.createdAt = NOW();'),
      await queryInterface.sequelize.query('CREATE TRIGGER seguridad_privacidad_updated_trigger_text'+
      ' BEFORE UPDATE ON seguridad_privacidad FOR EACH ROW SET NEW.updatedAt = NOW();')
    ]
  },
  down: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.sequelize.query('DROP TRIGGER seguridad_privacidad_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER seguridad_privacidad_updated_trigger_text'),
      await queryInterface.dropTable('seguridad_privacidad')
    ]
  }
};