import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface MunicipiosAttributes {
  id : number;
  clave: string;
  nombre: string;
  id_estado: number;
  activo: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface MunicipiosModel extends Model<MunicipiosAttributes>, MunicipiosAttributes {};
export class Municipios extends Model<MunicipiosModel, MunicipiosAttributes> {
};

export type MunicipiosStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): MunicipiosModel;
};

export const MunicipioFactory = (sequelize: Sequelize.Sequelize): MunicipiosStatic => {
  return <MunicipiosStatic>sequelize.define('municipios',{
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    clave: {
        type: DataTypes.STRING,
        allowNull: false
    },
    nombre: {
        type: DataTypes.STRING,
        allowNull: false
    },
    id_estado: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    activo: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: null
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: null
    }
  });
}