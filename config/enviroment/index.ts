import dotenv from "dotenv";
import path from "path";
var  admin  =  require("firebase-admin");

var  serviceAccount  =  require("../firebase.json");

admin.initializeApp({
  credencial : admin.credential.cert(serviceAccount),
  databaseURL: "https://alohaus-bb221-default-rtdb.firebaseio.com"
})

dotenv.config({
  path: path.resolve(__dirname, process.env.NODE_ENV + '.env')
});

const config = {
  NODE_ENV: process.env.NODE_ENV,
  PORT_EXPRESS: process.env.PORT || 3000,
  PORT_HTTP: 3001,
  redis: {
    DB_REDIS  : process.env.DB_REDIS,
    PORT_REDIS: parseInt(process.env.PORT_REDIS!) || 15490,
    HOST_REDIS: process.env.HOST_REDIS || 'redis-15490.c44.us-east-1-2.ec2.cloud.redislabs.com',
    PASS_REDIS: process.env.PASS_REDIS || '6wx7WhbR11bnBiJ7nhCnUIHEafDcmvsD',
  },
  twilio: {
    TWILIO_ACCOUNT_SID: process.env.TWILIO_ACCOUNT_SID,
    TWILIO_AUTH_TOKEN: process.env.TWILIO_AUTH_TOKEN,
    NUMBER_TWILIO: process.env.NUMBER_TWILIO
  },
  firebase: admin,
  JWT_SECRET_KEY: process.env.JWT_SECRET_KEY
}

export { config }
