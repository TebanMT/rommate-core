/**
 * @author Esteban Mendiola <mendiola_esteban@outlook.com>
*/

import Router from "express-promise-router";
import { UserMiddleware } from "../middleware/user-middleware";

export function UserRoutes({UserController}: any) {
  const router = Router();
  router.post('/',UserMiddleware.verifyExistUser,UserController.registerUser.bind(UserController));
  router.post('/login',UserController.login.bind(UserController));
  router.get('/:id',UserController.find.bind(UserController));
  return router;
};