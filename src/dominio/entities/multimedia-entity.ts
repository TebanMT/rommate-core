import { Espacio } from "./espacio-entity";

export interface Multimedia {
    id?: number;
    url: string;
    tipo: string;
    id_espacio: number;
  }