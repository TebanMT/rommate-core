import { asClass, createContainer, asFunction, asValue } from "awilix";
import { Server } from "./server";
import { CatalogosController, EspacioController, SMSController, UserController, NotificationsController } from "./controllers";
import { StartUp } from "./startup";
import { Routes } from "./routes";
import { config } from "../../../config/enviroment"
import { UserRoutes } from "./routes/user-routes";
import { SMSRoutes } from "./routes/sms-routes";
import { CatalogosRoutes } from "./routes/catalogos-routes";
import { EspacioRoutes } from "./routes/espacio-routes";
import { NotificationsRoutes } from "./routes/notifications-routes";

const container = createContainer();

container.register({
    app: asClass(StartUp).singleton(),
    server : asClass(Server).singleton(),
    router : asFunction(Routes).singleton(),

    //Routes
    UserRoutes      : asFunction(UserRoutes).singleton(),
    SMSRoutes       : asFunction(SMSRoutes).singleton(),
    CatalogosRoutes : asFunction(CatalogosRoutes).singleton(),
    EspacioRoutes   : asFunction(EspacioRoutes).singleton(),
    NotificationsRoutes : asFunction(NotificationsRoutes).singleton(),

    //Controllers
    UserController      : asClass(UserController).singleton(),
    SMSController       : asClass(SMSController).singleton(),
    CatalogosController : asClass(CatalogosController).singleton(),
    EspacioController   : asClass(EspacioController).singleton(),
    NotificationsController : asClass(NotificationsController).singleton(),

    //Config
    config: asValue(config),
});

module.exports = container;