import { SeguridadPrivacidad } from "../../../dominio/entities/catalogos-entity";
import { EspacioSeguridadPrivRepository } from "../../../dominio/ports";
import { EspacioSeguridadPriv } from "../orm";
import { EspacioSeguridadPrivAttributes } from "../orm/models/espacio-seguridad-priv-model";

export class EspacioSeguridadPrivacidadDBRepository implements EspacioSeguridadPrivRepository{
    async createEspacioSegPriv(seguridad_priv: SeguridadPrivacidad[], id_espacio: number): Promise<boolean> {
        const e: EspacioSeguridadPrivAttributes[] = [];
        await seguridad_priv.map((r) =>{ e.push({id_espacio:id_espacio,id_seguridad_priv:r.id})});
        try {
            await EspacioSeguridadPriv.bulkCreate(e);
            return true;
        } catch (error) {
            return false;
        }
    }
}