/**
 * @author Esteban Mendiola <mendiola_esteban@outlook.com>
*/

import Router from "express-promise-router";

export function EspacioRoutes({EspacioController}: any) {
    const router = Router();
    router.post('/',EspacioController.create.bind(EspacioController));
    router.get('/',EspacioController.getMinumunDataEspacios.bind(EspacioController));
    router.patch('/',EspacioController.changeActivo.bind(EspacioController));
    router.delete('/:id',EspacioController.deleteEspacio.bind(EspacioController));
    router.get('/cities/',EspacioController.getCountCities.bind(EspacioController));
    router.get('/:espacioID',EspacioController.getEspacio.bind(EspacioController));
    router.get('/:lugar/:tipo/:fecha/:id_anfitrion',EspacioController.getMinumunDataEspacios.bind(EspacioController));
    return router;
};