import { Estado, Municipio, Reglas, SeguridadPrivacidad, Servicios, TipoEspacio } from "../dominio/entities/catalogos-entity";
import { CatalogosRepository } from "../dominio/ports/catalogos-repository";

export class CatalogosService {

    private catalogoRepo: CatalogosRepository;

    constructor(CatalogosRepository: CatalogosRepository){
        this.catalogoRepo = CatalogosRepository;
    }

    async estadosActivos(activo: boolean): Promise<Estado[] | null> {
        return await this.catalogoRepo.getCatalogoActivosEstados(activo);
    }

    async municipiosActivos(idEstado: number, activo: boolean): Promise<Municipio[] | null> {
        return await this.catalogoRepo.getCatalogoActivosMunicipios(idEstado, activo);
    }

    async tipoEspaciosActivos(activo: boolean): Promise<TipoEspacio[] | null> {
        return await this.catalogoRepo.getCatalogoActivosTipoEspacio(activo);
    }

    async serviciosActivos(activo: boolean): Promise<Servicios[] | null> {
        return await this.catalogoRepo.getCatalogoActivosServicios(activo);
    }

    async seguridadActivos(activo: boolean): Promise<SeguridadPrivacidad[] | null> {
        return await this.catalogoRepo.getCatalogoActivosSeguridadPrivacidad(activo);
    }

    async reglasActivos(activo: boolean): Promise<Reglas[] | null> {
        return await this.catalogoRepo.getCatalogoActivosReglasEspacio(activo);
    }

}