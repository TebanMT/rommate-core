'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

   await queryInterface.bulkInsert('seguridad_privacidad',[{
    nombre: 'Entrada Privada',
    descripcion: '',
    activo: 1,
  },{
    nombre: 'Seguridad Privada',
    descripcion: '',
    activo: 1,
  },{
    nombre: 'Detectores de humo',
    descripcion: '',
    activo: 1,
  },{
    nombre: 'Botiquín de primeros auxilios',
    descripcion: '',
    activo: 1,
  },{
    nombre: 'Extintores',
    descripcion: '',
    activo: 1,
  }],{});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('seguridad_privacidad',null,{});
  }
};
