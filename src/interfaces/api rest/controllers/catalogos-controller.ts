import { Request, Response } from "express";
import { CatalogosDBRepository } from "../../../infrestructura/db/repositories/catalogos-db-repository";
import { CatalogosService } from "../../../servicios";

export class CatalogosController {
    /**
     * get estados activos
     */
    async estados(rer: Request, res: Response) {
        const repo    = new CatalogosDBRepository();
        const service = new CatalogosService(repo);
        try {
            const c = await service.estadosActivos(true);
            res.status(200).json({payload: c});
        } catch (error) {
            res.status(500).json({error: error});
        }
    }

    /**
     *  get municipios ligados a un estado
     */
    async municipios(req: Request, res: Response) {
        const repo    = new CatalogosDBRepository();
        const service = new CatalogosService(repo);
        try {
            const c = await service.municipiosActivos(parseInt(req.params.estadoID), true);
            res.status(200).json({payload: c});
        } catch (error) {
            res.status(500).json({error: error});
        }
    }

    /**
     * get tipo de espacios para rentar
     */
    async espacios(rer: Request, res: Response) {
        const repo    = new CatalogosDBRepository();
        const service = new CatalogosService(repo);
        try {
            const c = await service.tipoEspaciosActivos(true);
            res.status(200).json({payload: c});
        } catch (error) {
            res.status(500).json({error: error});
        }
    }

    async servicios(rer: Request, res: Response){
        const repo    = new CatalogosDBRepository();
        const service = new CatalogosService(repo);
        try {
            const c = await service.serviciosActivos(true);
            res.status(200).json({payload: c});
        } catch (error) {
            res.status(500).json({error: error});
        }
    }

    async seguridad(rer: Request, res: Response){
        const repo    = new CatalogosDBRepository();
        const service = new CatalogosService(repo);
        try {
            const c = await service.seguridadActivos(true);
            res.status(200).json({payload: c});
        } catch (error) {
            res.status(500).json({error: error});
        }
    }

    async reglas(rer: Request, res: Response){
        const repo    = new CatalogosDBRepository();
        const service = new CatalogosService(repo);
        try {
            const c = await service.reglasActivos(true);
            res.status(200).json({payload: c});
        } catch (error) {
            res.status(500).json({error: error});
        }
    }
}