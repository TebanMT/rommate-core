import { Multimedia } from "../dominio/entities/multimedia-entity";
import { MultimediaRepository } from "../dominio/ports";
import { writeFile } from "fs";
import sharp, { Sharp } from "sharp";

export class MultimediaService {

    private multimediaRepo : MultimediaRepository;

    constructor(multiRepo: MultimediaRepository){
        this.multimediaRepo = multiRepo;
    }

    async createMultimedia(id_espacio: number, data: any[]): Promise<boolean> {
        const saveDBdata: Multimedia[] = [];
        await data.map(async (m) => {
            const response = await this.decodeBase64Image(m.data, id_espacio);
            await saveDBdata.push({
                tipo: m.tipo,
                url: response.url,
                id_espacio: id_espacio,
            });
            console.log(response.url)
            await this.saveMultimedia(response.url, response.data);
        });
        console.log(saveDBdata)
        const result = await this.multimediaRepo.createMultimedia(saveDBdata, id_espacio);
        return result ? true : false;
    }

    async decodeBase64Image(dataString: string, id_espacio: number, tipo = "PHOTO"){
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response: any = {};
        const numero = Math.random();
        var ns = numero.toString();
        ns = ns.slice(2)
        if (matches!.length !== 3) {
          return new Error('Invalid input string');
        }
        response.type = matches![1].slice(6);
        response.data = new Buffer(matches![2], 'base64');
        response.data = await sharp(response.data).resize(1280, 720).toBuffer()
        response.url = '/home/estebanmendiola/Pictures/roommatePhotos/'+tipo+'_'+ns+'_'+id_espacio+'.'+response.type;
        return response;
    }

    async saveMultimedia(url: string, data: string): Promise<any>{
        try {
            await writeFile(url, data, err => {
                if (err) {
                    return err;
                }else{
                    return true;
                }
            });
        } catch (error) {
            return error;
        }
    }

}