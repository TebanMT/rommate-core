'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.createTable('espacio_seguridad_priv', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        id_espacio: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        id_seguridad_priv: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      await queryInterface.sequelize.query('CREATE TRIGGER espacio_seguridad_priv_created_trigger_text'+
      ' BEFORE INSERT ON espacio_seguridad_priv FOR EACH ROW SET NEW.createdAt = NOW();'),
      await queryInterface.sequelize.query('CREATE TRIGGER espacio_seguridad_priv_updated_trigger_text'+
      ' BEFORE UPDATE ON espacio_seguridad_priv FOR EACH ROW SET NEW.updatedAt = NOW();')
    ]
  },
  down: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.sequelize.query('DROP TRIGGER espacio_seguridad_priv_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER espacio_seguridad_priv_updated_trigger_text'),
      await queryInterface.dropTable('espacio_seguridad_priv')
    ]
  }
};