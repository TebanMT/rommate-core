export interface Estado {
    id: number;
    clave: string;
    nombre: string;
    abrev: string;
    activo: boolean;
}

export interface Municipio {
    id: number;
    clave: string;
    nombre: string;
    activo: boolean;
    id_estado: number;
}

export interface TipoEspacio {
    id: number;
    tipo: string;
    descripcion: string;
    activo: boolean;
}

export interface Servicios {
    id: number;
    servicio: string;
    descripcion: string;
    activo: boolean;
}

export interface SeguridadPrivacidad {
    id: number;
    nombre: string;
    descripcion: string;
    activo: boolean;
}

export interface Reglas {
    id: number;
    regla: string;
    descripcion: string;
    activo: boolean;
}