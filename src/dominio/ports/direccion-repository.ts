import {Direccion } from "../entities/direccion-entity";

export interface DireccionRepository {
    createDireccion(direccion: Direccion): Promise<Direccion>;
}