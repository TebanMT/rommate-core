import { Multimedia as MultimediaEntity } from "../../../dominio/entities/multimedia-entity";
import { MultimediaRepository } from "../../../dominio/ports";
import { MultimediaAttributes } from "../orm/models/multimedia-model";
import { Multimedia } from "../orm";

export class MultimediaDBRepository implements MultimediaRepository{
    async createMultimedia(multimedia: MultimediaEntity[], id_espacio: number): Promise<boolean> {
        console.log(multimedia)
        console.log(id_espacio)
        const e: MultimediaAttributes[] = [];
        await multimedia.map((r) =>{e.push({url:r.url,tipo:r.tipo,id_espacio:id_espacio})});
        try {
            await Multimedia.bulkCreate(e);
            return true;
        } catch (error) {
            console.log(error)
            return false;
        }
    }
}