import { Reglas } from "../../../dominio/entities/catalogos-entity";
import { EspacioReglasRepository } from "../../../dominio/ports";
import { EspacioReglas } from "../orm";
import { EspacioReglasAttributes} from "../orm/models/espacio-reglas-model";

export class EspacioReglasDBRepository implements EspacioReglasRepository{
    async createEspacioReglas(reglas: Reglas[], id_espacio: number): Promise<boolean> {
        const e: EspacioReglasAttributes[] = [];
        await reglas.map((r) =>{ e.push({id_espacio:id_espacio,id_regla:r.id})});
        try {
            await EspacioReglas.bulkCreate(e);
            return true;
        } catch (error) {
            return false;
        }
    }
}