'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

   await queryInterface.bulkInsert('servicios',[{
    servicio: 'Wi-fi',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Elementos Basicos',
    descripcion: 'Toallas, sabanas, jabón, papel higiénico, almohadas, etc.',
    activo: 1,
  },{
    servicio: 'Cocina',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Estacionamiento',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'TV',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Agua Caliente',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Calefacción',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Aire acondicionado',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Lavadora',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Lavadero',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Secadora',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Plancha',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Secador de pelo',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Alberca',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Microondas',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Estufa',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Utencilios para Cocinar',
    descripcion: '',
    activo: 1,
  },{
    servicio: 'Patio o Balcon',
    descripcion: '',
    activo: 1,
  }],{});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('servicios',null,{});
  }
};
