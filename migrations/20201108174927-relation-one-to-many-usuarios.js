'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return [
      await queryInterface.addConstraint('logins',{
        type: 'FOREIGN KEY',
        fields: ['id_usuario'],
        name: 'FK_usuarios_logins',
        references: {
          table: 'usuarios',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),
      await queryInterface.addConstraint('usuarios',{
        type: 'FOREIGN KEY',
        fields: ['id_tipo_registro'],
        name: 'FK_usuarios_tipo_registro',
        references: {
          table: 'tipo_registro',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      })
    ]
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return [
      await queryInterface.removeConstraint('logins', 'FK_usuarios_logins'),
      await queryInterface.removeConstraint('usuarios', 'FK_usuarios_tipo_registro')
    ]
  }
};
