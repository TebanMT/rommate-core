'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return[
      await queryInterface.createTable('estados', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        clave: {
          type: Sequelize.STRING,
          allowNull: false
        },
        nombre: {
          type: Sequelize.STRING,
          allowNull: false
        },
        abrev: {
          type: Sequelize.STRING,
          allowNull: false
        },
        activo: {
          type: Sequelize.BOOLEAN,
          allowNull: false
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      await queryInterface.sequelize.query('CREATE TRIGGER estados_created_trigger_text'+
      ' BEFORE INSERT ON estados FOR EACH ROW SET NEW.createdAt = NOW();'),
      await queryInterface.sequelize.query('CREATE TRIGGER estados_updated_trigger_text'+
      ' BEFORE UPDATE ON estados FOR EACH ROW SET NEW.updatedAt = NOW();')
    ]
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return [
      await queryInterface.sequelize.query('DROP TRIGGER estados_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER estados_updated_trigger_text'),
      await queryInterface.dropTable('estados'),
    ];
  }
};
