import { UserService } from "./user-service";
import { CodigoVerificacionService } from "./codigo-verifiacion-service";
import { CatalogosService } from "./catalogos-servicios";
import { EspacioService } from "./espacio-service";
import { MultimediaService } from "./multimedia-service";
import { MensajesService } from "./mensajes-service";

export {
    UserService,
    CodigoVerificacionService,
    CatalogosService,
    EspacioService,
    MultimediaService,
    MensajesService
}