'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('estados',[
    { clave: '01', nombre: 'Aguascalientes', abrev: 'Ags.', activo: '0' },
    { clave: '02', nombre: 'Baja California', abrev: 'BC', activo: '0' },
    { clave: '03', nombre: 'Baja California Sur', abrev: 'BCS', activo: '0'},
    { clave: '04', nombre: 'Campeche', abrev: 'Camp.', activo: '0' },
    {
      clave: '05',
      nombre: 'Coahuila de Zaragoza',
      abrev: 'Coah.',
      activo: '0'
    },
    { clave: '06', nombre: 'Colima', abrev: 'Col.', activo: '0' },
    { clave: '07', nombre: 'Chiapas', abrev: 'Chis.', activo: '0' },
    { clave: '08', nombre: 'Chihuahua', abrev: 'Chih.', activo: '0' },
    { clave: '09', nombre: 'Ciudad de México', abrev: 'CDMX', activo: '1' },
    { clave: '10', nombre: 'Durango', abrev: 'Dgo.', activo: '0' },
    { clave: '11', nombre: 'Guanajuato', abrev: 'Gto.', activo: '1' },
    { clave: '12', nombre: 'Guerrero', abrev: 'Gro.', activo: '0' },
    { clave: '13', nombre: 'Hidalgo', abrev: 'Hgo.', activo: '0' },
    { clave: '14', nombre: 'Jalisco', abrev: 'Jal.', activo: '1' },
    { clave: '15', nombre: 'México', abrev: 'Mex.', activo: '0' },
    {
      clave: '16',
      nombre: 'Michoacán de Ocampo',
      abrev: 'Mich.',
      activo: '0'
    },
    { clave: '17', nombre: 'Morelos', abrev: 'Mor.', activo: '0' },
    { clave: '18', nombre: 'Nayarit', abrev: 'Nay.', activo: '0' },
    { clave: '19', nombre: 'Nuevo León', abrev: 'NL', activo: '0' },
    { clave: '20', nombre: 'Oaxaca', abrev: 'Oax.', activo: '0' },
    { clave: '21', nombre: 'Puebla', abrev: 'Pue.', activo: '0' },
    { clave: '22', nombre: 'Querétaro', abrev: 'Qro.', activo: '1' },
    { clave: '23', nombre: 'Quintana Roo', abrev: 'Q. Roo', activo: '0' },
    { clave: '24', nombre: 'San Luis Potosí', abrev: 'SLP', activo: '0' },
    { clave: '25', nombre: 'Sinaloa', abrev: 'Sin.', activo: '0' },
    { clave: '26', nombre: 'Sonora', abrev: 'Son.', activo: '0' },
    { clave: '27', nombre: 'Tabasco', abrev: 'Tab.', activo: '0' },
    { clave: '28', nombre: 'Tamaulipas', abrev: 'Tamps.', activo: '0' },
    { clave: '29', nombre: 'Tlaxcala', abrev: 'Tlax.', activo: '0' },
    {
      clave: '30',
      nombre: 'Veracruz de Ignacio de la Llave',
      abrev: 'Ver.',
      activo: '0'
    },
    { clave: '31', nombre: 'Yucatán', abrev: 'Yuc.', activo: '0' },
    { clave: '32', nombre: 'Zacatecas', abrev: 'Zac.', activo: '0' }
],{});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('estados',null,{});
  }
};
