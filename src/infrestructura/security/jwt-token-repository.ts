import jwt from "jsonwebtoken";
import { TokenManager } from "../../servicios/security/tokenManager";
import { config } from "../../../config/enviroment";

export class JwtTokenRepository implements TokenManager{

    generate(payload: any):string {
        return jwt.sign(payload, config.JWT_SECRET_KEY!);
    }

    decode(accessToken: any): any {
        return jwt.verify(accessToken, config.JWT_SECRET_KEY!);
    }
    
}
