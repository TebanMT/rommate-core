export interface NotificaionesRepository {
    sendMensajeNotificacionTelefono(mensaje: any, token: any): Promise<boolean>;
}