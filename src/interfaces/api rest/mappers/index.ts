import { Espacio } from "../../../dominio/entities/espacio-entity";
import Usuario from "../../../dominio/entities/user-entity"

export class Mappers{

    static userToEntitieDomain(user: Usuario){
        const u = {
            id: user.id,
            nombre: user.nombre,
            apellidos: user.apellidos,
            email: user.email,
            lada_int: user.lada_int,
            numero_cel: user.numero_cel,
            usuario: user.usuario,
            anfitrion: user.anfitrion,
            url_ID_government: user.url_ID_government,
            token: user.token,
            created: user.fecha_creacion!
        }
        return u;
    }

    static espacioToespacioMinimunDTO(espacio: Espacio){
        const e = {
            id: espacio.id!,
            titulo: espacio.titulo!,
            cantidad_personas: espacio.cantidad_personas!,
            descripcion_espacio: espacio.descripcion_espacio!,
            precio_renta: espacio.precio_renta!,
            ciudad_estado: espacio.direccion?.ciudad!+", "+espacio.direccion?.estado!,
            url_multimedia: espacio.multimedia?.map(res => res.url),
            disponible : espacio.diponible,
        }
        return e;
    }
}