export interface Mensaje {
    id?: number;
    id_usuario_origen: number;
    id_usuario_destino: number;
    mensaje: string;
    tipo: string;
    entregado: boolean;
    fecha_dipositivo: string; //epoch format
}