import { Mensaje } from "../entities/mensajes-entity";

export interface MensajeRepository {
    saveMensaje(mensaje: Mensaje): Promise<boolean | number>;

    updateDelivery(id: any): Promise<boolean>;

    getMessagesNotRecived(id: any): Promise<Mensaje[] | null>;
}