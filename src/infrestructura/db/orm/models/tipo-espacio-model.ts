import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface TipoEspacioAttributes {
  id : number;
  tipo: string;
  descripcion: string;
  activo: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface TipoEspacioModel extends Model<TipoEspacioAttributes>, TipoEspacioAttributes {};
export class TipoEspacio extends Model<TipoEspacioModel, TipoEspacioAttributes> {
};

export type TipoEspacioStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): TipoEspacioModel;
};

export const TipoEspacioFactory = (sequelize: Sequelize.Sequelize): TipoEspacioStatic => {
  return <TipoEspacioStatic>sequelize.define('tipo_espacio',{
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    tipo: {
        type: DataTypes.STRING
    },
    descripcion: {
        type: DataTypes.TEXT
    },
    activo: {
        type: DataTypes.BOOLEAN
    },
    createdAt: {
        allowNull: false,
        type: DataTypes.DATE
    },
    updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
    }
    }, {
      freezeTableName: true
    });
}