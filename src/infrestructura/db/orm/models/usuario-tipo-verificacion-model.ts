import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface UsuarioTipoVerificacionAttributes {
  id : number;
  tipo: number;
  id_tipo_verificacion: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface UsuarioTipoVerificacionModel extends Model<UsuarioTipoVerificacionAttributes>, UsuarioTipoVerificacionAttributes {};
export class UsuarioTipoVerificacion extends Model<UsuarioTipoVerificacionModel, UsuarioTipoVerificacionAttributes> {
};

export type UsuarioTipoVerificacionStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): UsuarioTipoVerificacionModel;
};

export const UsuarioTipoVerificacionFactory = (sequelize: Sequelize.Sequelize): UsuarioTipoVerificacionStatic => {
  return <UsuarioTipoVerificacionStatic>sequelize.define('usuario_tipo_verificacion',{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    id_usuario: DataTypes.INTEGER,
    id_tipo_verificacion: DataTypes.INTEGER,
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  },{
    freezeTableName: true
  });
}