import { SeguridadPrivacidad } from "../entities/catalogos-entity";

export interface EspacioSeguridadPrivRepository {
    createEspacioSegPriv(seguridad_priv: SeguridadPrivacidad[], id_espacio:number): Promise<boolean>;
}