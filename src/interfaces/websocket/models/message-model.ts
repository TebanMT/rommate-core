export interface MessageResquest {
    type: string,
    from: string,
    to  : string;
    message: string,
    epoch: string
}
