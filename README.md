# Rommate CORE

## Requisitos

- Node.js v12
- Mysql 8.0.22

## ¿ Cómo ejecutar el proyecto en local ?

```
1.- npm install ó npm i  # Instala las dependencias
2.- npm run migrate      # Para realizar las migraciones y los seeders
3.- npm run migrate:undo:all   # Para deshacer todas las migraciones
3.- npm run dev             # Para ejectuar el proyecto en local
```

Recordar que para las migraciones, previamente se debe tener una base de datos llamada 'roommate' y un usuario llamado 'roommate' que tenga privilegios para acceder a la bd.

En el archivo 'config.js' que esta en la carpeta config, se debe colocar el password del usuario 'roommate' en el diccionario 'development'. 
