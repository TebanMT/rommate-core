'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.createTable('codigos_verificacion', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        codigo: {
          allowNull: false,
          type: Sequelize.STRING
        },
        numero_telefono: {
          allowNull: false,
          type: Sequelize.STRING
        },
        activo: {
          allowNull: false,
          type: Sequelize.BOOLEAN
        },
        tipo: {
          allowNull: false,
          type: Sequelize.STRING
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
    await queryInterface.sequelize.query('CREATE TRIGGER codigos_verificacion_created_trigger_text'+
      ' BEFORE INSERT ON codigos_verificacion FOR EACH ROW SET NEW.createdAt = NOW();'),
    await queryInterface.sequelize.query('CREATE TRIGGER codigos_verificacion_updated_trigger_text'+
      ' BEFORE UPDATE ON codigos_verificacion FOR EACH ROW SET NEW.updatedAt = NOW();')
  ]
  },
  down: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.sequelize.query('DROP TRIGGER codigos_verificacion_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER codigos_verificacion_updated_trigger_text'),
      await queryInterface.dropTable('codigos_verificacion')
    ]
  }
};