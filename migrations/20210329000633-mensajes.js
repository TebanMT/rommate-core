'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return[
      await queryInterface.createTable('mensajes', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        id_usuario_origen: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        id_usuario_destino: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        mensaje: {
          type: Sequelize.STRING,
          allowNull: false
        },
        tipo: {
          type: Sequelize.STRING,
          allowNull: false
        },
        fecha_dipositivo: {
          type: Sequelize.STRING,
          allowNull: false
        },
        entregado: {
          type: Sequelize.BOOLEAN,
          allowNull: false
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      await queryInterface.sequelize.query('CREATE TRIGGER mensajes_created_trigger_text'+
      ' BEFORE INSERT ON mensajes FOR EACH ROW SET NEW.createdAt = NOW();'),
      await queryInterface.sequelize.query('CREATE TRIGGER mensajes_updated_trigger_text'+
      ' BEFORE UPDATE ON mensajes FOR EACH ROW SET NEW.updatedAt = NOW();'),

      await queryInterface.addConstraint('mensajes',{
        type: 'FOREIGN KEY',
        fields: ['id_usuario_origen'],
        name: 'FK_mensajes_usuario_origen',
        references: {
          table: 'usuarios',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),

      await queryInterface.addConstraint('mensajes',{
        type: 'FOREIGN KEY',
        fields: ['id_usuario_destino'],
        name: 'FK_mensajes_usuario_destino',
        references: {
          table: 'usuarios',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),

    ]
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return [
      await queryInterface.removeConstraint('mensajes', 'FK_mensajes_usuario_origen'),
      await queryInterface.removeConstraint('mensajes', 'FK_mensajes_usuario_destino'),
      await queryInterface.sequelize.query('DROP TRIGGER mensajes_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER mensajes_updated_trigger_text'),
      await queryInterface.dropTable('mensajes'),
    ];
  }
};
