import { Servicios } from "../../../dominio/entities/catalogos-entity";
import { EspacioServiciosRepository } from "../../../dominio/ports";
import { EspacioServicios } from "../orm";
import { EspacioServicioAttributes } from "../orm/models/espacio-servicios-model";

export class EspacioServiciosDBRepository implements EspacioServiciosRepository{
    async createEspacioServicios(servicios: Servicios[], id_espacio: number): Promise<boolean> {
        const e: EspacioServicioAttributes[] = [];
        await servicios.map((r) =>{ e.push({id_espacio:id_espacio,id_servicio:r.id})});
        try {
            await EspacioServicios.bulkCreate(e);
            return true;
        } catch (error) {
            return false;
        }
    }
}