export interface Direccion{
    id : number;
    pais: string;
    estado: string;
    ciudad: boolean;
    colonia: string;
    calle: string;
    codigo_postal: number;
    numero_ext: number;
    numero_int: number;
    direccion_text: string;
    lat: string;
    long: string;
}