import { Reglas, SeguridadPrivacidad, Servicios, TipoEspacio } from "./catalogos-entity";
import { Direccion } from "./direccion-entity";
import { Multimedia } from "./multimedia-entity";
import Usuario from "./user-entity";

export interface Espacio {
    id                   : number;
    titulo               : string;
    precio_renta         : number;
    cantidad_personas    : number;
    descripcion_espacio? : string;
    descripcion_gral?    : string;
    diponible            : boolean;
    fecha_disponibilidad?: string;
    id_direccion         : number;
    id_tipo_espacio      : number;
    id_usuario           : number;
    direccion?           : Direccion;
    tipo_espacio?        : TipoEspacio;
    anfitrion?           : Usuario;
    multimedia?          : Multimedia[];
    reglas?              : Reglas[];
    servicios?           : Servicios[];
    seguridad_privacidad?: SeguridadPrivacidad[]
}
