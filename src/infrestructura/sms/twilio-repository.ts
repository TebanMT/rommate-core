import { SMSManager } from "../../servicios/sms/smsManager";
import { config } from "../../../config/enviroment";
import twilio, { Twilio } from "twilio";

export class TwilioRepository implements SMSManager{

    private twilio: Twilio

    constructor(){
        this.twilio = twilio(config.twilio.TWILIO_ACCOUNT_SID, config.twilio.TWILIO_AUTH_TOKEN)
    }

    async send(lada: string, number: string, codigo:string): Promise<any> {
        return await this.twilio.messages.create({
                body: 'Tu codigo de verificacion es: '+codigo,
                from: config.twilio.NUMBER_TWILIO,
                to: lada+number
        });
    }

}