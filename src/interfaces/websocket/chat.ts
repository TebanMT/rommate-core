import { Socket } from "socket.io";
import { RedisClient, createClient } from "redis";
import { config } from "../../../config/enviroment";
import { Mensaje } from "../../dominio/entities/mensajes-entity";
import { MensajeDBRepository } from "../../infrestructura/db/repositories/mensaje-db-reposiroty";
import { FirebaseRepository } from "../../infrestructura/notifications/firebase-repository";
import { UserDBRepository } from "../../infrestructura/db/repositories/user-db-repository";
import { MensajesService } from "../../servicios";

export class Chat{

    server: any;
    io: any;
    client: RedisClient;
    repoMensaje = new MensajeDBRepository();
    serviceMensaje = new MensajesService(this.repoMensaje);

    constructor({ server }: any){
        this.server = server;
        this.io = server.io;
        this.client = createClient(
            {
                port: config.redis.PORT_REDIS,
                host: config.redis.HOST_REDIS,
                auth_pass: config.redis.PASS_REDIS
            }
        )
    }

    start(){
        this.server.start();
    }

    chat(){
        this.io.on('connection', async (socket: Socket) => {
            const query = socket.handshake.query as any;
            const result = {message: {}, id: ''}
            if (query.user == undefined) {
                console.log("Hoal disconnect")
                socket.disconnect();
            }
            console.log("Connected");
            console.log(socket.id)
            console.log(query.user)
            this.client.set(query.user,socket.id);
            this.io.in(socket.id).emit('connectt',query.user);
            try {
                const mensajes = await this.serviceMensaje.getMessagesNotRecived(query.user);
                for (const m of mensajes) {
                    const mm = {from: m.id_usuario_origen, to: m.id_usuario_destino, message: m.mensaje, type:'', epoch:''}
                    result.message = mm
                    result.id = m.id
                    socket.emit('message_response', result);
                }
            } catch (error) {
                socket.emit('message_response', result);
            }

            socket.on('message_request', (res)=>{
                this.client.get(res.to,async function (err, value){
                    const mensaje = {id_usuario_origen: res.from, id_usuario_destino: res.to,
                    mensaje: res.message, tipo:"Text", entregado: false,
                    fecha_dipositivo: res.epoch.toString()} as Mensaje;
                    const repoMensaje = new MensajeDBRepository();
                    const repoNotificaion = new FirebaseRepository();
                    const repoUser = new UserDBRepository()
                    const serviceMensaje = new MensajesService(repoMensaje, repoNotificaion, repoUser);
                    const result = {message: res, id: ''}
                    try {
                        const id = await serviceMensaje.saveMensaje(mensaje);
                        result.id = id.toString();
                        socket.broadcast.to(value!).emit('message_response', result);
                    } catch (error) {
                        socket.broadcast.to(value!).emit('message_response', result);
                    }
                });
            });

            socket.on('received', async (res) =>{
                try {
                    await this.serviceMensaje.updateDelivery(res);
                } catch (error) {
                    console.log(error);
                }
            });

            socket.on('disconnect', () => {
                console.log('Client disconnected');
                //this.client.del(query.user);
                socket.disconnect();
            });


        });
    }
}