import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface EspacioSeguridadPrivAttributes {
  id? : number;
  id_espacio: number;
  id_seguridad_priv: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface EspacioSeguridadPrivModel extends Model<EspacioSeguridadPrivAttributes>, EspacioSeguridadPrivAttributes {};
export class EspacioSeguridadPriv extends Model<EspacioSeguridadPrivModel, EspacioSeguridadPrivAttributes> {
};

export type EspacioSeguridadPrivStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): EspacioSeguridadPrivModel;
};

export const EspacioSeguridadPrivFactory = (sequelize: Sequelize.Sequelize): EspacioSeguridadPrivStatic => {
  return <EspacioSeguridadPrivStatic>sequelize.define('espacio_seguridad_priv',{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    id_espacio: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    id_seguridad_priv: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  },{
    freezeTableName: true
  });
}