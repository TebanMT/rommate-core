export class StartUp{

    server:any

    constructor({ server }:any){
        this.server = server;
    }

    async start(){
        await this.server.start();
    }
}