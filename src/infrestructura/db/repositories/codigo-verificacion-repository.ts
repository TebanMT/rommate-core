import CodigoVerificacion from "../../../dominio/entities/codigo-verificacion-entity";
import { CodigoVerificacionRepository } from "../../../dominio/ports";
import { Mappers } from "../mappers";
import { CodigoVerifiacion } from "../orm";

export class CodigoVerifiacionDBRepository implements CodigoVerificacionRepository{

    async getCodigosVerifiacionByPhone(number: string): Promise<CodigoVerificacion[] | null>{
        var c: CodigoVerificacion[] | null;
        const codigoModel = await CodigoVerifiacion.findAll({
            where: {numero_telefono: number}
        });
        if (codigoModel.length > 0) {
            c = codigoModel.map(codigo => Mappers.codigoVerifiacionToEntitieDomain(codigo));
        }else{
            c = null;
        }
       return c;
    }

    async updateLastCodeById(id: number): Promise<boolean> {
        const codigoModel = await CodigoVerifiacion.update(
            {activo: false},
            {where:{id: id}})
        const res = codigoModel[0] > 0 ? true : false;
        return res;
    }

    async createCode(code: CodigoVerificacion): Promise<CodigoVerificacion> {
        const codigoModel = await CodigoVerifiacion.create(code);
        const c: CodigoVerificacion = Mappers.codigoVerifiacionToEntitieDomain(codigoModel);
        return c;
    }

    async getCodeValidCodeByPhone(code: CodigoVerificacion): Promise<CodigoVerificacion | null> {
        var c: CodigoVerificacion | null;
        const codigoModel =  await CodigoVerifiacion.findOne({
            where:
            {
                numero_telefono : code.numero_telefono,
                codigo          : code.codigo,
                activo          : 1,
            }
        });
        c = codigoModel ? Mappers.codigoVerifiacionToEntitieDomain(codigoModel): null;
        return c;
    }
}