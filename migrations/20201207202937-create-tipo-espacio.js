'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.createTable('tipo_espacio', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        tipo: {
          type: Sequelize.STRING
        },
        descripcion: {
          type: Sequelize.TEXT
        },
        activo: {
          type: Sequelize.BOOLEAN
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      await queryInterface.sequelize.query('CREATE TRIGGER tipo_espacio_created_trigger_text'+
      ' BEFORE INSERT ON tipo_espacio FOR EACH ROW SET NEW.createdAt = NOW();'),
      await queryInterface.sequelize.query('CREATE TRIGGER tipo_espacio_updated_trigger_text'+
      ' BEFORE UPDATE ON tipo_espacio FOR EACH ROW SET NEW.updatedAt = NOW();')
    ]
  },
  down: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.sequelize.query('DROP TRIGGER tipo_espacio_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER tipo_espacio_updated_trigger_text'),
      await queryInterface.dropTable('tipo_espacio')
    ]
  }
};