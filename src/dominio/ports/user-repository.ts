import Usuario from "../entities/user-entity";

export interface UserRepository {
    find(id: number): Promise<Usuario | null>;
    createUser(user: Usuario): Promise<Usuario>;
    getUserByPhone(lada:string, number:string): Promise<Usuario | null>;
    existUser(user: Usuario): Promise<boolean>;
    updateAnfirion(id_user: number, status: boolean): Promise<boolean>;
    getFcmToken(id_user: number): Promise<string | undefined>;
}