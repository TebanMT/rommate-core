import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface UserAttributes {
  id? : number;
  token_fcm?: string;
  nombre: string;
  apellidos: string;
  password: string;
  email?: string;
  lada_int: string;
  numero_cel: string;
  fecha_nacimieto: Date;
  usuario: boolean;
  anfitrion: boolean;
  url_ID_government?: string;
  id_tipo_registro: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface UserModel extends Model<UserAttributes>, UserAttributes {};
export class User extends Model<UserModel, UserAttributes> {
};

export type UserStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): UserModel;
};

export const UserFactory = (sequelize: Sequelize.Sequelize): UserStatic => {
  return <UserStatic>sequelize.define('usuarios',{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    token_fcm: {
      allowNull: true,
      type: DataTypes.STRING
    },
    nombre: {
      allowNull: false,
      type: DataTypes.STRING
    },
    apellidos: {
      allowNull: false,
      type: DataTypes.STRING
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING
    },
    email: {
      allowNull: true,
      unique: true,
      type: DataTypes.STRING
    },
    lada_int: {
      allowNull: false,
      type: DataTypes.STRING
    },
    numero_cel: {
      allowNull: false,
      unique: true,
      type: DataTypes.STRING
    },
    fecha_nacimieto: {
      allowNull: false,
      type: DataTypes.DATE
    },
    usuario: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    anfitrion: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    url_ID_government: {
      allowNull: true,
      type: DataTypes.BOOLEAN
    },
    id_tipo_registro: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  });
}