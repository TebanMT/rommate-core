'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return [
      await queryInterface.addConstraint('municipios',{
        type: 'FOREIGN KEY',
        fields: ['id_estado'],
        name: 'FK_estado_municipio',
        references: {
          table: 'estados',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),
    ]
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return [
      await queryInterface.removeConstraint('municipios', 'FK_estado_municipio'),
    ]
  }
};
