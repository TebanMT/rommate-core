'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.createTable('servicios', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        servicio: {
          type: Sequelize.STRING,
          allowNull: false
        },
        descripcion:{
          type: Sequelize.TEXT
        },
        activo:{
          type: Sequelize.BOOLEAN,
          allowNull: false
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      await queryInterface.sequelize.query('CREATE TRIGGER servicios_created_trigger_text'+
      ' BEFORE INSERT ON servicios FOR EACH ROW SET NEW.createdAt = NOW();'),
      await queryInterface.sequelize.query('CREATE TRIGGER servicios_updated_trigger_text'+
      ' BEFORE UPDATE ON servicios FOR EACH ROW SET NEW.updatedAt = NOW();')
    ]
  },
  down: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.sequelize.query('DROP TRIGGER servicios_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER servicios_updated_trigger_text'),
      await queryInterface.dropTable('servicios')
    ]
  }
};