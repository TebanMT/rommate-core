export default interface CodigoVerificacion {
    id: number;
    codigo: string;
    numero_telefono: string;
    activo: boolean;
    tipo: string;
}