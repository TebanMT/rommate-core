import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface ReglasAttributes {
  id : number;
  regla: string;
  descripcion: string;
  activo: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface ReglasModel extends Model<ReglasAttributes>, ReglasAttributes {};
export class Reglas extends Model<ReglasModel, ReglasAttributes> {
};

export type ReglasStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): ReglasModel;
};

export const ReglasFactory = (sequelize: Sequelize.Sequelize): ReglasStatic => {
  return <ReglasStatic>sequelize.define('reglas',{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    regla: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    descripcion: {
      type: DataTypes.TEXT,
    },
    activo: {
      type: DataTypes.BOOLEAN,
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  });
}