import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface LoginAttributes {
  id : number;
  ubicacion: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface LoginModel extends Model<LoginAttributes>, LoginAttributes {};
export class LoginReglas extends Model<LoginModel, LoginAttributes> {
};

export type LoginStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): LoginModel;
};

export const LoginFactory = (sequelize: Sequelize.Sequelize): LoginStatic => {
  return <LoginStatic>sequelize.define('login',{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    ubicacion: DataTypes.TEXT,
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  },{
    freezeTableName: true
  });
}