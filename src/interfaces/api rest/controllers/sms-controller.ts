import { Request, Response } from "express";
import CodigoVerificacion from "../../../dominio/entities/codigo-verificacion-entity";
import { CodigoVerifiacionDBRepository } from "../../../infrestructura/db/repositories/codigo-verificacion-repository";
import { TwilioRepository } from "../../../infrestructura/sms/twilio-repository";
import { CodigoVerificacionService } from "../../../servicios";
import { SMSService } from "../../../servicios/sms-service";

export class SMSController {

    async send(req:Request, res:Response){
        const {lada_int, numero_cel, tipo} = req.body;
        const numero_telefono = lada_int+numero_cel
        const body: CodigoVerificacion = {numero_telefono, tipo} as CodigoVerificacion;
        const repoSMS       = new TwilioRepository();
        const repoCodigo    = new CodigoVerifiacionDBRepository();
        const serviceSMS    = new SMSService(repoSMS);
        const serviceCodigo = new CodigoVerificacionService(repoCodigo);
        try {
            const codigo        = await serviceCodigo.createCodigoVerificacion(body);
            //const sms           = await serviceSMS.send(lada_int, numero_cel, codigo.codigo);
            console.log(codigo)
            res.status(200).json({payload: codigo.codigo});
        } catch (error) {
            res.status(500).json({error: error});
        }
    }

    async verifyCode(req: Request, res: Response){
        const body: CodigoVerificacion = req.body as CodigoVerificacion;
        const repoCodigo    = new CodigoVerifiacionDBRepository();
        const serviceCodigo = new CodigoVerificacionService(repoCodigo);
        try {
            const response = await serviceCodigo.verifyCode(body);
            if (response) {
                res.status(200).json({status:true,message:"codigo valido"})
            }else{
                res.status(200).json({status:false,message:"codigo no valido"})
            }
        } catch (error) {
            console.log(error);
            res.status(500).json({error: error});
        }
    }
}