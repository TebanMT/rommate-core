import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface TipoVerificacionAttributes {
  id : number;
  tipo: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface TipoVerificacionModel extends Model<TipoVerificacionAttributes>, TipoVerificacionAttributes {};
export class TipoVerificacion extends Model<TipoVerificacionModel, TipoVerificacionAttributes> {
};

export type TipoVerificacionStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): TipoVerificacionModel;
};

export const TipoVerificacionFactory = (sequelize: Sequelize.Sequelize): TipoVerificacionStatic => {
  return <TipoVerificacionStatic>sequelize.define('tipo_verificacion',{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    tipo: DataTypes.STRING,
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  },{
    freezeTableName: true
  });
}