import { createServer, Server as ServerHttp } from "http";
import { Server as ServerIO, Socket } from "socket.io";


export class WebSocket{

    config: any;
    httpServer: ServerHttp;
    io?: ServerIO;
    options?: {};

    constructor({config}:any){
        this.config = config;
        this.httpServer = createServer();
        this.setOptions();
        this.setSocketIO();
    }

    setOptions(){
        this.options = {
            cors:{
              origin: ["http://localhost:8100","http://localhost:8201"],
              methods: ["GET", "POST"],
              credentials: true
            }
        }
    }

    setSocketIO(){
        this.io = new ServerIO(this.httpServer,this.options)
    }

    start(){
        this.httpServer.listen(this.config.PORT_HTTP, () => {
            console.log("server http en el puerto: "+this.config.PORT_HTTP)
          });
    }
}