import express from 'express';

export class Server {

    config:any;
    express:any;

    constructor({config, router}:any){
        this.config = config;
        this.express = express();
        this.express.use(router);

    }

    start(){
        const http = this.express.listen(this.config.PORT_EXPRESS, () =>{
            const { port } = http.address();
            console.log("Servidor REST corriendo en el puerto: "+ port);
        });
    }

}