'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('reglas',[{
    regla: 'No Se Permiten Mascotas',
    descripcion: '',
    activo: 1,
  },{
    regla: 'No Se Permiten Fiestas o Eventos',
    descripcion: '',
    activo: 1,
  },{
    regla: 'No Se Permite Fumar',
    descripcion: '',
    activo: 1,
  }],{});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('reglas',null,{});
  }
};
