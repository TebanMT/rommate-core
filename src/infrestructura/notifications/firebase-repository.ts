import { config } from "../../../config/enviroment/index";
import { NotificaionesRepository } from "../../dominio/ports";

export class FirebaseRepository implements NotificaionesRepository{

    async sendMensajeNotificacionTelefono(message: any, token:any): Promise<boolean> {
        const nofity = {
            message:{
                notification:{
                title:"Te han enviado un mensaje",
                body: message.mensaje,
                sound:"default",
                click_action:"FCM_PLUGIN_ACTIVITY",
                icon:"fcm_push_icon"
            },
            data:{
                id_usuario_origen: message.id_usuario_origen,
                id_usuario_destino: message.id_usuario_destino,
                mensaje: message.mensaje,
                tipo: "Text",
                fecha_dipositivo: ""
            }
          },
           to: token,
           priority:"high",
           restricted_package_name:""
          }
        const options  =  {
            priority: "high" ,
            timeToLive : 60 * 60 * 24
        };
        try {
            console.log(nofity.to)
            const result = await config.firebase.messaging().sendToDevice(nofity.to, nofity.message, options);
            return result;
        } catch (error) {
            return false;
        }
        
    }
}