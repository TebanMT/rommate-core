'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.createTable('espacios', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        titulo: {
          type: Sequelize.TEXT,
          allowNull: false
        },
        precio_renta: {
          type: Sequelize.DECIMAL,
          allowNull: false
        },
        cantidad_personas: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        descripcion_espacio: {
          type: Sequelize.TEXT,
          allowNull: true
        },
        descripcion_gral: {
          type: Sequelize.TEXT,
          allowNull: true
        },
        diponible: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        fecha_disponibilidad: {
          type: Sequelize.DATEONLY,
          allowNull: true
        },
        id_direccion: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        id_tipo_espacio: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        id_usuario: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      await queryInterface.sequelize.query('CREATE TRIGGER espacios_created_trigger_text'+
      ' BEFORE INSERT ON espacios FOR EACH ROW SET NEW.createdAt = NOW();'),
      await queryInterface.sequelize.query('CREATE TRIGGER espacios_updated_trigger_text'+
      ' BEFORE UPDATE ON espacios FOR EACH ROW SET NEW.updatedAt = NOW();')
    ]
  },
  down: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.sequelize.query('DROP TRIGGER espacios_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER espacios_updated_trigger_text'),
      await queryInterface.dropTable('espacios')
    ]
  }
};