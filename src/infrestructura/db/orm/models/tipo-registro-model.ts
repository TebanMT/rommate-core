import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface TipoRegistroAttributes {
  id : number;
  tipo: string;
  descripcion: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface TipoRegistroModel extends Model<TipoRegistroAttributes>, TipoRegistroAttributes {};
export class TipoRegistro extends Model<TipoRegistroModel, TipoRegistroAttributes> {
};

export type TipoRegistroStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): TipoRegistroModel;
};

export const TipoRegistroFactory = (sequelize: Sequelize.Sequelize): TipoRegistroStatic => {
  return <TipoRegistroStatic>sequelize.define('multimedia',{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    tipo: DataTypes.STRING,
    descripcion: DataTypes.TEXT,
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  },{
    freezeTableName: true
  });
}