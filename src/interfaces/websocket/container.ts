import { asClass, createContainer, asFunction, asValue } from "awilix";
import { config } from "../../../config/enviroment";
import { Chat } from "./chat";
import { WebSocket } from "./server";
import { StartUp } from "./startup";

const container = createContainer();

container.register({

    app: asClass(StartUp).singleton(),
    server: asClass(WebSocket).singleton(),
    chat: asClass(Chat).singleton(),


    config: asValue(config),

});

module.exports = container;