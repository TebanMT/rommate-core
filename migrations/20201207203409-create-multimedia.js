'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.createTable('multimedia', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        url: {
          type: Sequelize.TEXT
        },
        tipo: {
          type: Sequelize.STRING,
          defaultValue : 'FOTO',
        },
        id_espacio: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      await queryInterface.sequelize.query('CREATE TRIGGER multimedia_created_trigger_text'+
      ' BEFORE INSERT ON multimedia FOR EACH ROW SET NEW.createdAt = NOW();'),
      await queryInterface.sequelize.query('CREATE TRIGGER multimedia_updated_trigger_text'+
      ' BEFORE UPDATE ON multimedia FOR EACH ROW SET NEW.updatedAt = NOW();')
    ]
  },
  down: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.sequelize.query('DROP TRIGGER multimedia_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER multimedia_updated_trigger_text'),
      await queryInterface.dropTable('multimedia')
    ]
  }
};