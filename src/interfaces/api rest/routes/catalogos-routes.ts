/**
 * @author Esteban Mendiola <mendiola_esteban@outlook.com>
*/

import Router from "express-promise-router";

export function CatalogosRoutes({CatalogosController}: any) {
    const router = Router();
    router.get('/estados',CatalogosController.estados.bind(CatalogosController));
    router.get('/municipios/:estadoID',CatalogosController.municipios.bind(CatalogosController));
    router.get('/espacios',CatalogosController.espacios.bind(CatalogosController));
    router.get('/servicios',CatalogosController.servicios.bind(CatalogosController));
    router.get('/seguridad',CatalogosController.seguridad.bind(CatalogosController));
    router.get('/reglas',CatalogosController.reglas.bind(CatalogosController));
  return router;
};