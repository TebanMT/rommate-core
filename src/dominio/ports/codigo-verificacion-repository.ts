import CodigoVerificacion from "../entities/codigo-verificacion-entity";

export interface CodigoVerificacionRepository {

    getCodigosVerifiacionByPhone(number: string): Promise<CodigoVerificacion[] | null>;

    updateLastCodeById(id: number): Promise<boolean>;

    createCode(code: CodigoVerificacion): Promise<CodigoVerificacion>;

    getCodeValidCodeByPhone(code: CodigoVerificacion): Promise<CodigoVerificacion | null>;
}