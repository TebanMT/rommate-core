import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface EspacioServicioAttributes {
  id? : number;
  id_espacio: number;
  id_servicio: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface EspacioServicioModel extends Model<EspacioServicioAttributes>, EspacioServicioAttributes {};
export class EspacioServicio extends Model<EspacioServicioModel, EspacioServicioAttributes> {
};

export type EspacioServicioStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): EspacioServicioModel;
};

export const EspacioServicioFactory = (sequelize: Sequelize.Sequelize): EspacioServicioStatic => {
  return <EspacioServicioStatic>sequelize.define('espacio_servicios',{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    id_espacio: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    id_servicio: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  });
}