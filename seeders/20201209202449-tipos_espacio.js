'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

   await queryInterface.bulkInsert('tipo_espacio',[{
    tipo: 'CUARTO',
    descripcion: '',
    activo: 1,
  },{
    tipo: 'DEPARTAMENTO',
    descripcion: '',
    activo: 1,
  },{
    tipo: 'CASA',
    descripcion: '',
    activo: 1,
  },{
    tipo: 'VIVIENDA ANEXA',
    descripcion: '',
    activo: 1,
  }],{});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('tipo_espacio',null,{});
  }
};
