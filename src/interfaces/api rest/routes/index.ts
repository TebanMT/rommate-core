import Router from "express-promise-router";
import morgan  from "morgan";
import cors from "cors";
import bodyParser from "body-parser";
import express from 'express'

export function Routes({UserRoutes, SMSRoutes, CatalogosRoutes, EspacioRoutes, NotificationsRoutes}:any) {
    const router = Router();
    const publicDir = require('path').join('/');

    router
    .use(cors())
    .use(bodyParser.json({limit: "50mb"}))
    .use(morgan('dev'))
    .use(express.json({limit: '50mb'}))
    .use(express.urlencoded({limit: '50mb', extended: true }))
    .use(express.static(publicDir));

    //router.use("/",(req,res)=>{res.json({message:"AloHaus"})})
    router.use("/api/sms", SMSRoutes);
    router.use("/api/user", UserRoutes);
    router.use('/api/catalogos', CatalogosRoutes);
    router.use('/api/espacio', EspacioRoutes);
    router.use('/api/notify', NotificationsRoutes)

    return router;
}