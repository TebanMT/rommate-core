'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('tipo_registro',[{
     tipo: 'LOCAL',
     descripcion: 'Se registra con numero de telefono y correo',
   },{
     tipo: 'FB',
     descripcion: 'Se registra con su cuenta de Facebook',
   },{
     tipo: 'GOOGLE',
     descripcion: 'Se registra con su cuenta de correo Gmail',
   }],{});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('tipo_registro',null,{});
  }
};
