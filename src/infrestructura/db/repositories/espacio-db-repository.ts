import sequelize from "sequelize";
import { Op } from "sequelize";
import { Espacio as EspacioEntity } from "../../../dominio/entities/espacio-entity";
import { EspacioRepository } from "../../../dominio/ports";
import { Mappers } from "../mappers";
import { Direccion, Espacio, Multimedia, User, TipoEspacio, Reglas, Servicios, SeguridadPrivacidad } from "../orm";
import { EspacioModel } from "../orm/models/espacio-model";

export class EspacioDBRepository implements EspacioRepository{

    async getMinumunDataEspacios(lugar?:string, tipo?:string, fecha?:string): Promise<EspacioEntity[] | null> {
        console.log(lugar, tipo, fecha)
        try {
            const espacioModel: EspacioModel[] = await Espacio.findAll({
                include: [
                    { model: Direccion, as: 'direccion', where:{ciudad: lugar ? lugar : {[Op.not]: null} }},
                    { model: Multimedia, as: 'multimedia' }
                ],
                where: { diponible: true, id_tipo_espacio: tipo ? tipo : {[Op.not]: null}, fecha_disponibilidad: fecha ? fecha : { [Op.or]: [ {[Op.not]: null}, {[Op.is]: null} ]}  },
            });
            const espacioEntity = espacioModel.map(e => Mappers.espacioToEntitieDomain(e));
            return espacioEntity;
        } catch (error) {
            console.log(error)
            return null;
        }
    }

    async getMinumunDataEspaciosByAnfitrion(id_anfitrion:string): Promise<EspacioEntity[] | null> {
        try {
            const espacioModel: EspacioModel[] = await Espacio.findAll({
                include: [
                    { model: Direccion, as: 'direccion'},
                    { model: Multimedia, as: 'multimedia' }
                ],
                where: { id_usuario: id_anfitrion},
            });
            const espacioEntity = espacioModel.map(e => Mappers.espacioToEntitieDomain(e));
            return espacioEntity;
        } catch (error) {
            console.log(error)
            return null;
        }
    }

    async getEspacio(id: number): Promise<EspacioEntity | null> {
        const espacioModel = await Espacio.findOne({
            where: { id: id},
            include: [
                { model: Direccion, as: 'direccion' },
                { model: TipoEspacio, as: 'tipo_espacio'},
                { model: User, as: 'anfitrion'},
                { model: Reglas, as: 'reglas'},
                { model: Servicios, as: 'servicios'},
                { model: SeguridadPrivacidad, as: 'seguridad_privacidad'}
            ]
        });
        var espacioEntity:any = null;
        if (espacioModel) {
            espacioEntity = Mappers.espacioToEntitieDomain(espacioModel);
        }
        return espacioEntity;
    }

    async createEspacio(espacio: EspacioEntity): Promise<EspacioEntity> {
        const espacioModel = await Espacio.create(espacio);
        return Mappers.espacioToEntitieDomain(espacioModel);
    }

    async getCountCities(): Promise<any[]> {
        const res: any[] = []
        const count = await Direccion.findAll({
            attributes: ['ciudad','estado',[sequelize.fn('count', sequelize.col('ciudad')),'amount']],
            include: [{model: Espacio, as:'espacios',include:[]}],
            group: ['ciudad']
        });
        count.map((e) => {
            const a = {ciudad: e.ciudad, estado:e.estado, amount:e.get('amount')};
            res.push(a)
        })
        return res;
    }

    changeActivo(id_anfitrion:number, status:boolean): Promise<boolean>{
        return new Promise<boolean>((resolve, reject) => {
            Espacio.update({diponible:status},{where:{id:id_anfitrion}})
            .then(_ => resolve(true)).catch(err => reject(err))
        });
    }

    deleteEspacio(id_espacio: number): Promise<boolean>{
        return new Promise<boolean>((resolve, reject) => {
            Espacio.destroy({
                where: {
                    id : id_espacio
                }
            }).then(_ => resolve(true)).catch(err => reject(err))
        })
    }


}