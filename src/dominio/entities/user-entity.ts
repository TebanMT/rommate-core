export default interface Usuario {
    id: number;
    token_fcm?: string;
    nombre: string;
    apellidos: string;
    password: string;
    email: string;
    lada_int: string;
    numero_cel: string;
    fecha_nacimieto: Date;
    usuario: boolean;
    anfitrion: boolean;
    url_ID_government: string;
    id_tipo_registro: number;
    token?: string;
    fecha_creacion?: string;
}