import { UserController } from "./user-controller";
import { SMSController } from "./sms-controller";
import { CatalogosController } from "./catalogos-controller";
import { EspacioController } from "./espacio-controller";
import { NotificationsController } from "./notifications-controller";

export {
    UserController,
    SMSController,
    CatalogosController,
    EspacioController,
    NotificationsController
}