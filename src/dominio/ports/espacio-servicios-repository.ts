import { Servicios } from "../entities/catalogos-entity";

export interface EspacioServiciosRepository {
    createEspacioServicios(servicios: Servicios[], id_espacio: number): Promise<boolean>;
}