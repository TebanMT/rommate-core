'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.createTable('logins', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        ubicacion: {
          type: Sequelize.TEXT
        },
        id_usuario: {
          allowNull: false,
          type: Sequelize.INTEGER
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
    }),
    await queryInterface.sequelize.query('CREATE TRIGGER logins_created_trigger_text'+
      ' BEFORE INSERT ON logins FOR EACH ROW SET NEW.createdAt = NOW();'),
    await queryInterface.sequelize.query('CREATE TRIGGER logins_updated_trigger_text'+
      ' BEFORE UPDATE ON logins FOR EACH ROW SET NEW.updatedAt = NOW();')
  ]
  },
  down: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.sequelize.query('DROP TRIGGER logins_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER logins_updated_trigger_text'),
      await queryInterface.dropTable('logins')
    ]
  }
};