'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return [
      await queryInterface.addConstraint('usuario_tipo_verificacion',{
        type: 'FOREIGN KEY',
        fields: ['id_usuario'],
        name: 'FK_usuario_tipo_verificacion_usuario',
        references: {
          table: 'usuarios',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),
      await queryInterface.addConstraint('usuario_tipo_verificacion',{
        type: 'FOREIGN KEY',
        fields: ['id_tipo_verificacion'],
        name: 'FK_usuario_tipo_verificacion_tipo',
        references: {
          table: 'tipo_verificacion',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      })
    ]
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return [
      await queryInterface.removeConstraint('usuario_tipo_verificacion', 'FK_usuario_tipo_verificacion_usuario'),
      await queryInterface.removeConstraint('usuario_tipo_verificacion', 'FK_usuario_tipo_verificacion_tipo')
    ]
  }
};
