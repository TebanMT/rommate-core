import { CodigoVerifiacionModel } from "../orm/models/codigos-verifiacion-model";
import { DireccionModel } from "../orm/models/direccion-model";
import { EspacioAttributes, EspacioModel, EspacioStatic } from "../orm/models/espacio-model";
import { MensajesModel } from "../orm/models/mensajes-model";
import { MultimediaModel } from "../orm/models/multimedia-model";
import { ReglasModel } from "../orm/models/reglas-models";
import { SeguridadPrivacidadModel } from "../orm/models/seguridad-privacidad-model";
import { ServiciosModel } from "../orm/models/servicios-model";
import { TipoEspacioModel } from "../orm/models/tipo-espacio-model";
import { UserModel } from "../orm/models/usuario-model";

export class Mappers{

    static userToEntitieDomain(userModel: UserModel){
        const u = {
            id: userModel.id!,
            token_fcm: userModel.token_fcm,
            nombre: userModel.nombre,
            apellidos: userModel.apellidos,
            password: userModel.password,
            email: userModel.email!,
            lada_int: userModel.lada_int!,
            numero_cel: userModel.numero_cel,
            fecha_nacimieto: userModel.fecha_nacimieto,
            usuario: userModel.usuario,
            anfitrion: userModel.anfitrion,
            url_ID_government: userModel.url_ID_government!,
            id_tipo_registro: userModel.id_tipo_registro,
            fecha_creacion: userModel.createdAt?.toISOString(),
        }
        return u
    }

    static codigoVerifiacionToEntitieDomain(codigoModel: CodigoVerifiacionModel){
        const c = {
            id: codigoModel.id!,
            codigo: codigoModel.codigo,
            numero_telefono: codigoModel.numero_telefono,
            activo: codigoModel.activo,
            tipo: codigoModel.tipo
        }
        return c;
    }

    static direccionToEntitieDomain(direccion: DireccionModel){
        const d ={
            id : direccion.id,
            pais: direccion.pais,
            estado: direccion.estado,
            ciudad: direccion.ciudad,
            colonia: direccion.colonia,
            calle: direccion.calle,
            codigo_postal: direccion.codigo_postal,
            numero_ext: direccion.numero_ext,
            numero_int: direccion.numero_int,
            direccion_text: direccion.direccion_text,
            lat: direccion.lat,
            long: direccion.long
        }
        return d;
    }

    static tipoEspacioToEntitieDomain(tipo_espacio: TipoEspacioModel){
        const t = {
            id: tipo_espacio.id,
            tipo: tipo_espacio.tipo,
            descripcion: tipo_espacio.descripcion,
            activo: tipo_espacio.activo
        }
        return t;
    }

    static multimediaToEntitieDomain(multimedia: MultimediaModel){
        const m ={
            id : multimedia.id!,
            url: multimedia.url,
            tipo: multimedia.tipo,
            id_espacio: multimedia.id_espacio
        }
        return m;
    }

    static reglasToEntitieDomain(reglas: ReglasModel){
        const r = {
            id: reglas.id,
            regla: reglas.regla,
            descripcion: reglas.descripcion,
            activo: reglas.activo
        }
        return r;
    }

    static serviciosToEntitieDomain(servicio: ServiciosModel){
        const s = {
            id: servicio.id,
            servicio: servicio.servicio,
            descripcion: servicio.descripcion,
            activo: servicio.activo
        }
        return s;
    }

    static seguridadPrivToEntitieDomain(seguridad_priv: SeguridadPrivacidadModel){
        const sp = {
            id: seguridad_priv.id,
            nombre: seguridad_priv.nombre,
            descripcion: seguridad_priv.descripcion,
            activo: seguridad_priv.activo
        }
        return sp;
    }

    static espacioToEntitieDomain(espacio: EspacioModel | any){
        const e = {
            id                   : espacio.id,
            titulo               : espacio.titulo,
            precio_renta         : espacio.precio_renta,
            cantidad_personas    : espacio.cantidad_personas,
            descripcion_espacio  : espacio.descripcion_espacio,
            descripcion_gral     : espacio.descripcion_gral,
            diponible            : espacio.diponible,
            fecha_disponibilidad : espacio.fecha_disponibilidad,
            id_direccion         : espacio.id_direccion,
            id_tipo_espacio      : espacio.id_tipo_espacio,
            id_usuario           : espacio.id_usuario,
            direccion            : espacio.direccion ? this.direccionToEntitieDomain(espacio.direccion) : undefined,
            tipo_espacio         : espacio.tipo_espacio ? this.tipoEspacioToEntitieDomain(espacio.tipo_espacio) : undefined,
            anfitrion            : espacio.anfitrion ? this.userToEntitieDomain(espacio.anfitrion) : undefined,
            multimedia           : espacio.multimedia ? espacio.multimedia.map( (m:any) => this.multimediaToEntitieDomain(m)) : undefined,
            reglas               : espacio.reglas ? espacio.reglas.map((r:any) => this.reglasToEntitieDomain(r)) : undefined,
            servicios            : espacio.servicios ? espacio.servicios.map((s:any) => this.serviciosToEntitieDomain(s)) : undefined,
            seguridad_privacidad : espacio.seguridad_privacidad ? espacio.seguridad_privacidad.map((sp:any) => this.seguridadPrivToEntitieDomain(sp)) : undefined
        }
        return e;
    }

    static mensajeToEntitieDomain(message: MensajesModel | any){
        const m = {
            id: message.id,
            id_usuario_origen: message.id_usuario_origen,
            id_usuario_destino: message.id_usuario_destino,
            mensaje: message.mensaje,
            tipo: message.tipo,
            entregado: message.entregado,
            fecha_dipositivo: message.fecha_dipositivo
        }
        return m
    }
}