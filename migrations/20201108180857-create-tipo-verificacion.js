'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.createTable('tipo_verificacion', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        tipo: {
          type: Sequelize.STRING
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
    await queryInterface.sequelize.query('CREATE TRIGGER tipo_verificacion_created_trigger_text'+
      ' BEFORE INSERT ON tipo_verificacion FOR EACH ROW SET NEW.createdAt = NOW();'),
    await queryInterface.sequelize.query('CREATE TRIGGER tipo_verificacion_updated_trigger_text'+
      ' BEFORE UPDATE ON tipo_verificacion FOR EACH ROW SET NEW.updatedAt = NOW();')
    ]
  },
  down: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.sequelize.query('DROP TRIGGER tipo_verificacion_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER tipo_verificacion_updated_trigger_text'),
      await queryInterface.dropTable('tipo_verificacion')
    ]
  }
};