import { Mensaje } from "../dominio/entities/mensajes-entity";
import { MensajeRepository, NotificaionesRepository, UserRepository } from "../dominio/ports";

export class MensajesService{

    private mensajeRepo: MensajeRepository;
    private notificacionRepo?: NotificaionesRepository;
    private userRepo?: UserRepository;

    constructor(mensajeRepo: MensajeRepository, notificacionRepo?: NotificaionesRepository, userRepo?: UserRepository){
        this.mensajeRepo = mensajeRepo;
        this.notificacionRepo = notificacionRepo;
        this.userRepo = userRepo;
    }

    async saveMensaje(mensaje: Mensaje) {
        const id = await this.mensajeRepo.saveMensaje(mensaje);
        if(id){
            const token = await this.userRepo?.getFcmToken(mensaje.id_usuario_destino);
            const n =await this.notificacionRepo?.sendMensajeNotificacionTelefono(mensaje, token);
            console.log(n)
            return id;
        }else{
            throw new Error("Error al guardar el mensaje");
        }
    }

    async updateDelivery(id: any){
        const res = await this.mensajeRepo.updateDelivery(id);
        return res
    }

    async getMessagesNotRecived(id: any){
        try {
            const mensaejes = await this.mensajeRepo.getMessagesNotRecived(id);
            return mensaejes;
        } catch (error) {
            return error;
        }

    }
}