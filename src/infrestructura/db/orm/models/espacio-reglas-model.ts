import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface EspacioReglasAttributes {
  id? : number;
  id_espacio: number;
  id_regla: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface EspacioReglasModel extends Model<EspacioReglasAttributes>, EspacioReglasAttributes {};
export class EspacioReglas extends Model<EspacioReglasModel, EspacioReglasAttributes> {
};

export type EspacioReglasStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): EspacioReglasModel;
};

export const EspacioReglasFactory = (sequelize: Sequelize.Sequelize): EspacioReglasStatic => {
  return <EspacioReglasStatic>sequelize.define('espacio_reglas',{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    id_espacio: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    id_regla: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  });
}