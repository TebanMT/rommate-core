import { Reglas } from "../entities/catalogos-entity";

export interface EspacioReglasRepository {
    createEspacioReglas(reglas: Reglas[], id_espacio:number): Promise<boolean>;
}