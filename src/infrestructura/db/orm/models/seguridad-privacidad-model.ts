import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface SeguridadPrivacidadAttributes {
  id : number;
  nombre: string;
  descripcion: string;
  activo: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface SeguridadPrivacidadModel extends Model<SeguridadPrivacidadAttributes>, SeguridadPrivacidadAttributes {};
export class SeguridadPrivacidad extends Model<SeguridadPrivacidadModel, SeguridadPrivacidadAttributes> {
};

export type SeguridadPrivacidadStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): SeguridadPrivacidadModel;
};

export const SeguridadPrivacidadFactory = (sequelize: Sequelize.Sequelize): SeguridadPrivacidadStatic => {
  return <SeguridadPrivacidadStatic>sequelize.define('seguridad_privacidad',{
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      nombre: {
        type: DataTypes.STRING,
        allowNull: false
      },
      descripcion: {
        type: DataTypes.TEXT
      },
      activo: {
        type: DataTypes.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    },{
        freezeTableName: true
    });
}