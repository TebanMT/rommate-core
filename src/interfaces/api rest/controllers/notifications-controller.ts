import { Request, Response } from "express";
import { MensajeDBRepository } from "../../../infrestructura/db/repositories/mensaje-db-reposiroty";
import { MensajesService } from "../../../servicios";
import { Mensaje } from "../../../dominio/entities/mensajes-entity";
import { FirebaseRepository } from "../../../infrestructura/notifications/firebase-repository";
import { UserDBRepository } from "../../../infrestructura/db/repositories/user-db-repository";

export class NotificationsController{

    async sendNotification(req: Request, res: Response){
        const notificacion   = req.body;
        const mensaje = req.body.message.data as Mensaje;
        const repoMensaje = new MensajeDBRepository();
        const repoNotificaion = new FirebaseRepository();
        const repoUser = new UserDBRepository()
        const serviceMensaje = new MensajesService(repoMensaje, repoNotificaion, repoUser);
        //console.log(mensaje)
        try {
            const result = await serviceMensaje.saveMensaje(mensaje);
            //console.log(result)
            res.status(200).json({payload: "Notification sent successfully"});
        } catch (error) {
            res.status(500).json({error: error});
        }
        
    }
}