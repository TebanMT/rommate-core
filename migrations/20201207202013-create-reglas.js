'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.createTable('reglas', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        regla: {
          type: Sequelize.TEXT,
          allowNull: false
        },
        descripcion: {
          type: Sequelize.TEXT,
        },
        activo: {
          type: Sequelize.BOOLEAN,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      await queryInterface.sequelize.query('CREATE TRIGGER reglas_created_trigger_text'+
      ' BEFORE INSERT ON reglas FOR EACH ROW SET NEW.createdAt = NOW();'),
      await queryInterface.sequelize.query('CREATE TRIGGER reglas_updated_trigger_text'+
      ' BEFORE UPDATE ON reglas FOR EACH ROW SET NEW.updatedAt = NOW();')
    ]
  },
  down: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.sequelize.query('DROP TRIGGER reglas_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER reglas_updated_trigger_text'),
      await queryInterface.dropTable('reglas')
    ]
  }
};