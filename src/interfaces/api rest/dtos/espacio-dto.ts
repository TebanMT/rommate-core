export default interface espacioMinimun{
    id: number;
    titulo: string;
    cantidad_personas: number;
    descripcion_espacio: string;
    precio_renta: number;
    ciudad_estado: string;
    url_multimedia?: string[];
}