'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */

    return [
      // Many to Many ----- espacio_servicios
      await queryInterface.addConstraint('espacio_servicios',{
        type: 'FOREIGN KEY',
        fields: ['id_espacio'],
        name: 'FK_espacio_espacio_servicios',
        references: {
          table: 'espacios',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),
      await queryInterface.addConstraint('espacio_servicios',{
        type: 'FOREIGN KEY',
        fields: ['id_servicio'],
        name: 'FK_servicio_espacio_servicios',
        references: {
          table: 'servicios',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),

      // Many to Many ----- espacio_seguridad_priv
      await queryInterface.addConstraint('espacio_seguridad_priv',{
        type: 'FOREIGN KEY',
        fields: ['id_espacio'],
        name: 'FK_espacio_espacio_seguridad_priv',
        references: {
          table: 'espacios',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),
      await queryInterface.addConstraint('espacio_seguridad_priv',{
        type: 'FOREIGN KEY',
        fields: ['id_seguridad_priv'],
        name: 'FK_seguridad_priv_espacio_seguridad_priv',
        references: {
          table: 'seguridad_privacidad',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),

      // Many to Many ----- espacio_seguridad_priv
      await queryInterface.addConstraint('espacio_reglas',{
        type: 'FOREIGN KEY',
        fields: ['id_espacio'],
        name: 'FK_espacio_espacio_reglas',
        references: {
          table: 'espacios',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),
      await queryInterface.addConstraint('espacio_reglas',{
        type: 'FOREIGN KEY',
        fields: ['id_regla'],
        name: 'FK_regla_espacio_reglas',
        references: {
          table: 'reglas',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),

      // One to Many --- Espacio_multimedia
      await queryInterface.addConstraint('multimedia',{
        type: 'FOREIGN KEY',
        fields: ['id_espacio'],
        name: 'FK_espacio_multimedia',
        references: {
          table: 'espacios',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),

      // One to Many --- Tipo_espacio_espacio
      await queryInterface.addConstraint('espacios',{
        type: 'FOREIGN KEY',
        fields: ['id_tipo_espacio'],
        name: 'FK_tipo_espacio_espacios',
        references: {
          table: 'tipo_espacio',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),

      // One to Many --- ubicacion_espacio
      await queryInterface.addConstraint('espacios',{
        type: 'FOREIGN KEY',
        fields: ['id_direccion'],
        name: 'FK_direccion_espacio',
        references: {
          table: 'direccion',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),

      // One to Many --- espacio_usuario
      await queryInterface.addConstraint('espacios',{
        type: 'FOREIGN KEY',
        fields: ['id_usuario'],
        name: 'FK_usuario_espacio',
        references: {
          table: 'usuarios',
          field: 'id',
        },
        onDelete: 'CASCADE',
        onUpdate: 'no action',
      }),
    ]
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return [
      await queryInterface.removeConstraint('espacio_servicios', 'FK_espacio_espacio_servicios'),
      await queryInterface.removeConstraint('espacio_servicios', 'FK_servicio_espacio_servicios'),
      await queryInterface.removeConstraint('espacio_seguridad_priv', 'FK_espacio_espacio_seguridad_priv'),
      await queryInterface.removeConstraint('espacio_seguridad_priv', 'FK_seguridad_priv_espacio_seguridad_priv'),
      await queryInterface.removeConstraint('espacio_reglas', 'FK_espacio_espacio_reglas'),
      await queryInterface.removeConstraint('espacio_reglas', 'FK_regla_espacio_reglas'),
      await queryInterface.removeConstraint('multimedia', 'FK_espacio_multimedia'),
      await queryInterface.removeConstraint('espacios', 'FK_tipo_espacio_espacios'),
      await queryInterface.removeConstraint('espacios', 'FK_direccion_espacio'),
      await queryInterface.removeConstraint('espacios', 'FK_usuario_espacio'),
    ]
  }
};
