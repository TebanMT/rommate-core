import { Estado, Municipio,
    TipoEspacio as TipoEspacioDomain,
    Servicios as ServiciosDomain,
    SeguridadPrivacidad as SeguridadPrivacidadDomain,
    Reglas as ReglasEntity } from "../../../dominio/entities/catalogos-entity";
import { CatalogosRepository } from "../../../dominio/ports/catalogos-repository";
import { Estados, Municipios, Reglas, SeguridadPrivacidad, Servicios, TipoEspacio } from "../orm";

export class CatalogosDBRepository implements CatalogosRepository{

    async getCatalogoActivosEstados(activo?: boolean): Promise<Estado[] | null> {
        const e = await Estados.findAll({
            where:
            {
                activo : activo,
            }
        });
        return e;
    }
    async getCatalogoActivosMunicipios(idEstado: number, activo?: boolean): Promise<Municipio[] | null> {
        const e = await Municipios.findAll({
            where:
            {
                activo    : activo,
                id_estado : idEstado
            }
        });
        return e;
    }
    async getCatalogoActivosTipoEspacio(activo?: boolean): Promise<TipoEspacioDomain[] | null> {
        const e = await TipoEspacio.findAll({
            where:
            {
                activo : activo,
            }
        });
        return e;
    }
    async getCatalogoActivosServicios(activo?: boolean): Promise<ServiciosDomain[] | null> {
        const e = await Servicios.findAll({
            where:
            {
                activo : activo,
            }
        });
        return e;
    }
    async getCatalogoActivosSeguridadPrivacidad(activo?: boolean): Promise<SeguridadPrivacidadDomain[] | null> {
        const e = await SeguridadPrivacidad.findAll({
            where:
            {
                activo : activo,
            }
        });
        return e;
    }
    async getCatalogoActivosReglasEspacio(activo?: boolean): Promise<ReglasEntity[] | null> {
        const e = await Reglas.findAll({
            where:
            {
                activo : activo,
            }
        });
        return e;
    }
}
