
export class StartUp{

    chat: any;

    constructor({chat}: any){
        this.chat = chat;
    }

    async start(){
        await this.chat.start();
        await this.chat.chat();
    }
}