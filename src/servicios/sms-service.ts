import { SMSManager } from "./sms/smsManager";

export class SMSService {

    private smsRepo: SMSManager;

    constructor(SMSRepository:SMSManager){
        this.smsRepo = SMSRepository;
    }

    async send(lada: string, number:string, codigo?: string): Promise<any> {
        return await this.smsRepo.send(lada,number,codigo);
    }


}