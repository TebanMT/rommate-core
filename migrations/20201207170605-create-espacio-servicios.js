'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.createTable('espacio_servicios', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        id_espacio: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        id_servicio: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      await queryInterface.sequelize.query('CREATE TRIGGER espacio_servicios_created_trigger_text'+
      ' BEFORE INSERT ON espacio_servicios FOR EACH ROW SET NEW.createdAt = NOW();'),
      await queryInterface.sequelize.query('CREATE TRIGGER espacio_servicios_updated_trigger_text'+
      ' BEFORE UPDATE ON espacio_servicios FOR EACH ROW SET NEW.updatedAt = NOW();')
    ]
  },
  down: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.sequelize.query('DROP TRIGGER espacio_servicios_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER espacio_servicios_updated_trigger_text'),
      await queryInterface.dropTable('espacio_servicios')
    ]
  }
};