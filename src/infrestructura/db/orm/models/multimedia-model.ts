import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';
import { EspacioModel } from './espacio-model';

export interface MultimediaAttributes {
  id? : number;
  url: string;
  tipo: string;
  id_espacio: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface MultimediaModel extends Model<MultimediaAttributes>, MultimediaAttributes {};
export class Multimedia extends Model<MultimediaModel, MultimediaAttributes> {
};

export type MultimediaStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): MultimediaModel;
};

export const MultimediaFactory = (sequelize: Sequelize.Sequelize): MultimediaStatic => {
  return <MultimediaStatic>sequelize.define('multimedia',{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    url: {
      type: DataTypes.TEXT
    },
    tipo: {
      type: DataTypes.STRING,
      defaultValue : 'FOTO',
    },
    id_espacio: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  },{
    freezeTableName: true
  });
}