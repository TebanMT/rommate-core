'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.createTable('usuarios', {

        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        token_fcm: {
          allowNull: true,
          type: Sequelize.STRING
        },
        nombre: {
          allowNull: false,
          type: Sequelize.STRING
        },
        apellidos: {
          allowNull: false,
          type: Sequelize.STRING
        },
        password: {
          allowNull: false,
          type: Sequelize.STRING
        },
        email: {
          allowNull: true,
          unique: true,
          type: Sequelize.STRING
        },
        lada_int: {
          allowNull: false,
          type: Sequelize.STRING
        },
        numero_cel: {
          allowNull: false,
          unique: true,
          type: Sequelize.STRING
        },
        fecha_nacimieto: {
          allowNull: false,
          type: Sequelize.DATE
        },
        usuario: {
          allowNull: false,
          type: Sequelize.BOOLEAN
        },
        anfitrion: {
          allowNull: false,
          type: Sequelize.BOOLEAN
        },
        url_ID_government: {
          allowNull: true,
          type: Sequelize.TEXT
        },
        id_tipo_registro: {
          allowNull: false,
          type: Sequelize.INTEGER
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
      }),
      await queryInterface.sequelize.query('CREATE TRIGGER usuario_created_trigger_text'+
      ' BEFORE INSERT ON usuarios FOR EACH ROW SET NEW.createdAt = NOW();'),
      await queryInterface.sequelize.query('CREATE TRIGGER usuario_updated_trigger_text'+
      ' BEFORE UPDATE ON usuarios FOR EACH ROW SET NEW.updatedAt = NOW();')
    ]
  },
  down: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.sequelize.query('DROP TRIGGER usuario_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER usuario_updated_trigger_text'),
      await queryInterface.dropTable('usuarios'),
    ];
  }
};