import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface DireccionAttributes {
  id : number;
  pais: string;
  estado: string;
  ciudad: boolean;
  colonia: string;
  calle: string;
  codigo_postal: number;
  numero_ext: number;
  numero_int: number;
  direccion_text: string;
  lat: string;
  long: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface DireccionModel extends Model<DireccionAttributes>, DireccionAttributes {};
export class Direccion extends Model<DireccionModel, DireccionAttributes> {
};

export type DireccionStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): DireccionModel;
};

export const DireccionFactory = (sequelize: Sequelize.Sequelize): DireccionStatic => {
  return <DireccionStatic>sequelize.define('direccion',{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    pais: {
      type: DataTypes.STRING,
      allowNull: false
    },
    estado: {
      type: DataTypes.STRING,
      allowNull: false
    },
    ciudad: {
      type: DataTypes.STRING,
      allowNull: false
    },
    colonia: {
      type: DataTypes.STRING,
      allowNull: true
    },
    calle: {
      type: DataTypes.STRING,
      allowNull: false
    },
    codigo_postal: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    numero_ext: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    numero_int: {
      type: DataTypes.INTEGER,
    },
    direccion_text: {
      type: DataTypes.STRING,
    },
    lat: {
      type: DataTypes.TEXT
    },
    long:{
      type: DataTypes.TEXT
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    freezeTableName: true
  });
}