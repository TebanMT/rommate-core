import * as sequelize from "sequelize";
import { UserFactory } from "./models/usuario-model";
import { CodigoVerifiacionFactory } from "./models/codigos-verifiacion-model";
import { DireccionFactory } from "./models/direccion-model";
import { EspacioFactory } from "./models/espacio-model";
import { EspacioReglasFactory } from "./models/espacio-reglas-model";
import { EspacioSeguridadPrivFactory } from "./models/espacio-seguridad-priv-model";
import { EspacioServicioFactory } from "./models/espacio-servicios-model";
import { EstadoFactory } from "./models/estado-model";
import { LoginFactory } from "./models/login-model";
import { MultimediaFactory } from "./models/multimedia-model";
import { MunicipioFactory } from "./models/municipio-model";
import { ReglasFactory } from "./models/reglas-models";
import { SeguridadPrivacidadFactory } from "./models/seguridad-privacidad-model";
import { ServiciosFactory } from "./models/servicios-model";
import { TipoEspacioFactory } from "./models/tipo-espacio-model";
import { TipoRegistroFactory } from "./models/tipo-registro-model";
import { TipoVerificacionFactory } from "./models/tipo-verificacion-model";
import { UsuarioTipoVerificacionFactory } from "./models/usuario-tipo-verificacion-model";
import * as configdb from '../../../../config/config.json';
import { MensajeFactory } from "./models/mensajes-model";

const dbconfig = process.env.NODE_ENV == 'production' ? configdb.production : configdb.development;

export const dbConfig = new sequelize.Sequelize(
    (process.env.DB_NAME = dbconfig.database),
    (process.env.DB_USER = dbconfig.username),
    (process.env.DB_PASSWORD = dbconfig.password),
    {
        //port: Number(process.env.DB_PORT) || 54320,
        host: dbconfig.host || "localhost",
        dialect: "mysql",
        pool: {
            min: 0,
            max: 5,
            acquire: 30000,
            idle: 10000,
        },
    }
);

// SOMETHING VERY IMPORTANT them Factory functions expect a
// sequelize instance as parameter give them `dbConfig`

export const User                   = UserFactory(dbConfig);
export const CodigoVerifiacion      = CodigoVerifiacionFactory(dbConfig);
export const Direccion              = DireccionFactory(dbConfig);
export const Espacio                = EspacioFactory(dbConfig);
export const EspacioReglas          = EspacioReglasFactory(dbConfig);
export const EspacioSeguridadPriv   = EspacioSeguridadPrivFactory(dbConfig)
export const EspacioServicios       = EspacioServicioFactory(dbConfig);
export const Estados                = EstadoFactory(dbConfig);
export const Login                  = LoginFactory(dbConfig);
export const Multimedia             = MultimediaFactory(dbConfig);
export const Municipios             = MunicipioFactory(dbConfig);
export const Reglas                 = ReglasFactory(dbConfig);
export const SeguridadPrivacidad    = SeguridadPrivacidadFactory(dbConfig);
export const Servicios              = ServiciosFactory(dbConfig);
export const TipoEspacio            = TipoEspacioFactory(dbConfig);
export const TipoVerificacion       = TipoVerificacionFactory(dbConfig);
export const TipoRegistro           = TipoRegistroFactory(dbConfig);
export const UsuarioTipoVerificacion= UsuarioTipoVerificacionFactory(dbConfig);
export const Mensajes               = MensajeFactory(dbConfig);

// Direccion´s relationships

Direccion.hasMany(Espacio,{
    foreignKey: 'id_direccion',
    as: 'espacios'
});

// Espacio´s relationships, it has many

Espacio.belongsTo(User,{
    foreignKey: 'id_usuario',
    as: "anfitrion",
});

Espacio.belongsTo(Direccion,{
    foreignKey: 'id_direccion',
    as: "direccion",
});

Espacio.belongsTo(TipoEspacio,{
    foreignKey: 'id_tipo_espacio',
    as: "tipo_espacio",
});

Espacio.hasMany(Multimedia,{
    foreignKey: 'id_espacio',
    as: 'multimedia'
});

Espacio.belongsToMany(Reglas,{
    through: 'espacio_reglas',
    as: 'reglas',
    foreignKey: 'id_espacio'
});

Espacio.belongsToMany(SeguridadPrivacidad,{
    through: 'espacio_seguridad_priv',
    as: 'seguridad_privacidad',
    foreignKey: 'id_espacio'
});

Espacio.belongsToMany(Servicios,{
    through: 'espacio_servicios',
    as: 'servicios',
    foreignKey: 'id_espacio'
});

// User relationships

User.belongsTo(TipoRegistro,{
    foreignKey: 'id_tipo_registro',
    as: 'tipo_registro',
});

User.belongsToMany(TipoVerificacion,{
    through: 'usuario_tipo_verificacion',
    as: 'tipo_verificacion',
    foreignKey: 'id_usuario',
});

User.hasMany(Espacio,{
    foreignKey: 'id_usuario',
    as: "espacios"
});

User.hasMany(Login,{
    foreignKey: 'id_usuario',
    as: 'logins'
});

// Login´s relatioships

Login.belongsTo(User,{
    foreignKey: 'id_usuario',
    as: 'usuarios'
});

// Multimedia´s relationships

Multimedia.belongsTo(Espacio,{
    foreignKey: 'id_espacio',
    as: 'espacio'
});

// Reglas´s relationships

Reglas.belongsToMany(Espacio,{
    through: 'espacio_reglas',
    as: 'espacios',
    foreignKey: 'id_regla'
});

// Seguriada_privacidad´s relationships

SeguridadPrivacidad.belongsToMany(Espacio,{
    through: 'espacio_seguridad_priv',
    as: 'espacios',
    foreignKey: 'id_seguridad_priv'
});

// Servicios´s relationships

Servicios.belongsToMany(Espacio,{
    through: 'espacio_servicios',
    as: 'espacios',
    foreignKey: 'id_servicio'
});

// TipoEspacio´s relationships

TipoEspacio.hasMany(Espacio,{
    foreignKey: 'id_tipo_espacio',
    as: 'espacios'
});

// TipoRegistro´s relationships

TipoRegistro.hasMany(User,{
    foreignKey: 'id_tipo_registro',
    as: 'usuarios'
});

// TipoVerifiacion´s relationships

TipoVerificacion.belongsToMany(User,{
    through: 'usuario_tipo_verificacion',
    as: 'usuarios',
    foreignKey: 'id_tipo_verificacion',
});

// Mensajes´s relationships

Mensajes.belongsTo(User,{
    foreignKey: 'id_usuario_origen',
    as: 'usuario_origen'
});

Mensajes.belongsTo(User,{
    foreignKey: 'id_usuario_destino',
    as: 'usuario_destino'
});

// the skill is the limit!