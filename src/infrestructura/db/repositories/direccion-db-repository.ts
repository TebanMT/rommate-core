import {Direccion as DireccionEntity } from "../../../dominio/entities/direccion-entity";
import { DireccionRepository } from "../../../dominio/ports";
import { Direccion } from "../orm";

export class DireccionDBRepository implements DireccionRepository{

    async createDireccion(direccion: DireccionEntity): Promise<DireccionEntity> {
        const dirModel = await Direccion.create(direccion);
        return dirModel;
    }
}