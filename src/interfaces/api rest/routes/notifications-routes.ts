/**
 * @author Esteban Mendiola <mendiola_esteban@outlook.com>
*/

import Router from "express-promise-router";

export function NotificationsRoutes({NotificationsController}: any) {
  const router = Router();
  router.post('/',NotificationsController.sendNotification.bind(NotificationsController));
  return router;
};