/**
 * @author Esteban Mendiola <mendiola_esteban@outlook.com>
*/

import { NextFunction, Request, Response } from "express";
import Usuario from "../../../dominio/entities/user-entity";
import { UserDBRepository } from "../../../infrestructura/db/repositories/user-db-repository";
import { UserService } from "../../../servicios";


export class UserMiddleware {

    static async verifyExistUser(req: Request, res: Response, next: NextFunction){
        const userDomain: Usuario = req.body;
        const repo = new UserDBRepository();
        try {
            const result = await new UserService(repo).existUser(userDomain);
            if (result) {
                // True means user exist, therefor user not will be create
                res.status(400).json({payload: {message : "No se puede crear el Usuario, el numero de telefono ya existe o el email."}});
            } else {
                //False means user no exist, therefor user will be create
                next();
            }
        } catch (error) {
            res.status(500).json(error)
        }
    }
}
