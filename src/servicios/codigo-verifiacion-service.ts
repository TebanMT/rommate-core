import CodigoVerificacion from "../dominio/entities/codigo-verificacion-entity";
import { CodigoVerificacionRepository } from "../dominio/ports";

export class CodigoVerificacionService {

    private codigoRepo: CodigoVerificacionRepository;

    constructor(CodigoVerificacionRepository: CodigoVerificacionRepository){
        this.codigoRepo = CodigoVerificacionRepository;
    }

    async getCodigosVerifiacionByPhone(number: string): Promise<CodigoVerificacion[] | null> {
        return await this.codigoRepo.getCodigosVerifiacionByPhone(number);
    }

    async createCodigoVerificacion(codigo: CodigoVerificacion): Promise<CodigoVerificacion>{
        const hasCode = await this.getCodigosVerifiacionByPhone(codigo.numero_telefono);
        if (hasCode != null) {
            /* In this context, if getCodigosVerifiacionByPhone does return not null,
               it means that the phone has already had at least one code, therefor
               the last code must be disabled */
            await this.codigoRepo.updateLastCodeById(hasCode[hasCode.length - 1].id)
        }
        const code = Math.floor(Math.random() * (9999 - 1000)) + 1000;
        codigo.codigo = code.toString();
        codigo.activo = true;
        return await this.codigoRepo.createCode(codigo);
    }

    async verifyCode(codigo: CodigoVerificacion): Promise<boolean>{
        const code = await this.codigoRepo.getCodeValidCodeByPhone(codigo);
        return code ? true : false;
    }


}