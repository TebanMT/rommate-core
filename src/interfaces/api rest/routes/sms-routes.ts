/**
 * @author Esteban Mendiola <mendiola_esteban@outlook.com>
*/

import Router from "express-promise-router";

export function SMSRoutes({SMSController}: any) {
  const router = Router();
  router.post('/',SMSController.send.bind(SMSController));
  router.post('/verify', SMSController.verifyCode.bind(SMSController));
  return router;
};