import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface EstadosAttributes {
  id : number;
  clave: string;
  nombre: string;
  abrev: string;
  activo: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface EstadosModel extends Model<EstadosAttributes>, EstadosAttributes {};
export class Estados extends Model<EstadosModel, EstadosAttributes> {
};

export type EstadosStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): EstadosModel;
};

export const EstadoFactory = (sequelize: Sequelize.Sequelize): EstadosStatic => {
  return <EstadosStatic>sequelize.define('estados',{
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    clave: {
        type: DataTypes.STRING,
        allowNull: false
    },
    nombre: {
        type: DataTypes.STRING,
        allowNull: false
    },
    abrev: {
        type: DataTypes.STRING,
        allowNull: false
    },
    activo: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: null
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: null
    }
  });
}