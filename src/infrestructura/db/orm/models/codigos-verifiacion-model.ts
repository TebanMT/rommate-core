import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface CodigoVerifiacionAttributes {
  id? : number;
  codigo: string;
  numero_telefono: string;
  activo: boolean;
  tipo: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface CodigoVerifiacionModel extends Model<CodigoVerifiacionAttributes>, CodigoVerifiacionAttributes {};
export class User extends Model<CodigoVerifiacionModel, CodigoVerifiacionAttributes> {
};

export type CodigoVerifiacionStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): CodigoVerifiacionModel;
};

export const CodigoVerifiacionFactory = (sequelize: Sequelize.Sequelize): CodigoVerifiacionStatic => {
  return <CodigoVerifiacionStatic>sequelize.define('codigos_verificacion',{
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    codigo: {
        allowNull: false,
        type: DataTypes.STRING
    },
    numero_telefono: {
        allowNull: false,
        type: DataTypes.STRING
    },
    activo: {
        allowNull: false,
        type: DataTypes.BOOLEAN
    },
    tipo: {
        allowNull: false,
        type: DataTypes.STRING
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },{
    freezeTableName: true,
  });
}