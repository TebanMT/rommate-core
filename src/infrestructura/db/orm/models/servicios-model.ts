import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface ServiciosAttributes {
  id : number;
  servicio: string;
  descripcion: string;
  activo: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface ServiciosModel extends Model<ServiciosAttributes>, ServiciosAttributes {};
export class Servicios extends Model<ServiciosModel, ServiciosAttributes> {
};

export type ServiciosStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): ServiciosModel;
};

export const ServiciosFactory = (sequelize: Sequelize.Sequelize): ServiciosStatic => {
  return <ServiciosStatic>sequelize.define('servicios',{
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    servicio: {
        type: DataTypes.STRING,
        allowNull: false
    },
    descripcion:{
        type: DataTypes.TEXT
    },
    activo:{
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    createdAt: {
        allowNull: false,
        type: DataTypes.DATE
    },
    updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
    }
    });
}