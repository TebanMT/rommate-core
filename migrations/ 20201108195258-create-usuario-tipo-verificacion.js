'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.createTable('usuario_tipo_verificacion', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        id_usuario: {
          type: Sequelize.INTEGER
        },
        id_tipo_verificacion: {
          type: Sequelize.INTEGER
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
    }),
    await queryInterface.sequelize.query('CREATE TRIGGER usuario_tipo_verificacion_created_trigger_text'+
      ' BEFORE INSERT ON usuario_tipo_verificacion FOR EACH ROW SET NEW.createdAt = NOW();'),
    await queryInterface.sequelize.query('CREATE TRIGGER usuario_tipo_verificacion_updated_trigger_text'+
      ' BEFORE UPDATE ON usuario_tipo_verificacion FOR EACH ROW SET NEW.updatedAt = NOW();')
  ]
  },
  down: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.sequelize.query('DROP TRIGGER usuario_tipo_verificacion_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER usuario_tipo_verificacion_updated_trigger_text'),
      await queryInterface.dropTable('usuario_tipo_verificacion'),
    ]
  }
};