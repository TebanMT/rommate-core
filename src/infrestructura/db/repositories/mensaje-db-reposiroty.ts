import { Mensaje } from "../../../dominio/entities/mensajes-entity";
import { MensajeRepository } from "../../../dominio/ports";
import { Mappers } from "../mappers";
import { Mensajes } from "../orm";
import { MensajesModel } from "../orm/models/mensajes-model";

export class MensajeDBRepository implements MensajeRepository{

    async saveMensaje(mensaje: Mensaje): Promise<boolean | number>{
        const mensajeModel = await Mensajes.create(mensaje);
        if(mensajeModel)
            return mensajeModel.id!;
        else
            return false;
    }

    async updateDelivery(id: any): Promise<boolean> {
        const MensajeReturn = await Mensajes.update(
            {entregado : true},
            {where: {id : id}}
        );
        return MensajeReturn ? true : false;;
    }

    async getMessagesNotRecived(id: any): Promise<Mensaje[] | null> {
        const mensajes: MensajesModel[] = await Mensajes.findAll({
            where: {id_usuario_destino: parseInt(id), entregado: false}
        });
        const mensajesEntity = mensajes.map(e => Mappers.mensajeToEntitieDomain(e));
        return mensajesEntity;
    }
}