export interface TokenManager{
    generate(payload: any): string;
    decode(accessToken: any): any;
}