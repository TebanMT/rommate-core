import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes } from 'sequelize';

export interface MensajessAttributes {
  id?: number;
  id_usuario_origen: number;
  id_usuario_destino: number;
  mensaje: string;
  tipo: string;
  entregado: boolean;
  fecha_dipositivo: string; //epoch format
  createdAt?: Date;
  updatedAt?: Date;
}

export interface MensajesModel extends Model<MensajessAttributes>, MensajessAttributes {};
export class Mensajes extends Model<MensajesModel, MensajessAttributes> {
};

export type MensajesStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): MensajesModel;
};

export const MensajeFactory = (sequelize: Sequelize.Sequelize): MensajesStatic => {
  return <MensajesStatic>sequelize.define('mensajes',{
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    id_usuario_origen: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    id_usuario_destino: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    mensaje: {
        type: DataTypes.STRING,
        allowNull: false
    },
    tipo: {
        type: DataTypes.STRING,
        allowNull: false
    },
    entregado: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    fecha_dipositivo: {
        type: DataTypes.STRING,
        allowNull: false
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: null
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: null
    }
  });
}