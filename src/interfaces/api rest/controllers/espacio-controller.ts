import { Request, Response } from "express";
import { Reglas, SeguridadPrivacidad, Servicios } from "../../../dominio/entities/catalogos-entity";
import { Direccion } from "../../../dominio/entities/direccion-entity";
import { Espacio } from "../../../dominio/entities/espacio-entity";
import { Multimedia } from "../../../dominio/entities/multimedia-entity";
import { DireccionDBRepository } from "../../../infrestructura/db/repositories/direccion-db-repository";
import { EspacioDBRepository } from "../../../infrestructura/db/repositories/espacio-db-repository";
import { EspacioReglasDBRepository } from "../../../infrestructura/db/repositories/espacio-reglas-db-repository";
import { EspacioSeguridadPrivacidadDBRepository } from "../../../infrestructura/db/repositories/espacio-seguridad-db-repository";
import { EspacioServiciosDBRepository } from "../../../infrestructura/db/repositories/espacio-servicios-db-repository";
import { MultimediaDBRepository } from "../../../infrestructura/db/repositories/multimedia-db-repository";
import { UserDBRepository } from "../../../infrestructura/db/repositories/user-db-repository";
import { EspacioService, MultimediaService } from "../../../servicios";
import espacioMinimun from "../dtos/espacio-dto";
import { Mappers } from "../mappers";

export class EspacioController {


    async getMinumunDataEspacios(req: Request, res: Response){
        let lugar = req.params.lugar != 'null' ? req.params.lugar : undefined;
        let tipo = req.params.tipo != 'null' ? req.params.tipo : undefined;
        let fecha = req.params.fecha != 'null' ? req.params.fecha : undefined;
        let id_anfitrion = req.params.id_anfitrion != 'null' ? req.params.id_anfitrion : undefined;
        const dto: espacioMinimun[] = [];
        const repo = new EspacioDBRepository();
        const service = new EspacioService(repo);
        try {
            const result = await service.getMinumunDataEspacios(lugar, tipo, fecha, id_anfitrion);
            await result?.map(r =>{dto.push(Mappers.espacioToespacioMinimunDTO(r))});
            res.status(200).json({payload: dto})
        } catch (error) {
            res.status(500).json({error: error});
        }
    }

    async create(req: Request, res: Response){
        // Data
        const espacio: Espacio               = req.body.espacio  as Espacio;
        const dir: Direccion                 = req.body.direccion as Direccion;
        const reglas: Reglas[]               = req.body.reglas as Reglas[];
        const segPriv: SeguridadPrivacidad[] = req.body.seguridad_priv as SeguridadPrivacidad[];
        const servicios: Servicios[]         = req.body.servicios as Servicios[];
        const multimedia: any[]              = req.body.multimedia as any[];

        // Repositories
        const eRepo          = new EspacioDBRepository();
        const dirRepo        = new DireccionDBRepository();
        const espRegRepo     = new EspacioReglasDBRepository();
        const eSegPrivRepo   = new EspacioSeguridadPrivacidadDBRepository();
        const eServiciosRepo = new EspacioServiciosDBRepository();
        const multimediaRepo = new MultimediaDBRepository();
        const userRepo       = new UserDBRepository();
        //

        const serviceE = new EspacioService(eRepo, dirRepo, espRegRepo, eSegPrivRepo, eServiciosRepo, userRepo);
        const serviceM = new MultimediaService(multimediaRepo);
        try {
            const result = await serviceE.createEspacio(espacio,dir,reglas,segPriv,servicios);
            await serviceM.createMultimedia(result.id, multimedia)
            res.status(200).json({payload: result})
        } catch (error) {
            console.log(error)
            res.status(500).json({error: error});
        }
    }

    async getEspacio(req: Request, res: Response){
        const idEspacio = parseInt(req.params.espacioID);
        const repo      = new EspacioDBRepository();
        const serviceE = new EspacioService(repo);
        try {
            const result = await serviceE.getEspacio(idEspacio);
            res.status(200).json({payload: result})
        } catch (error) {
            console.log(error)
            res.status(500).json({error: error});
        }
    }

    async getCountCities(req: Request, res: Response){
        const repo      = new EspacioDBRepository();
        const serviceE = new EspacioService(repo);
        try {
            const result = await serviceE.getCountCities();
            res.status(200).json({payload: result})
        } catch (error) {
            res.status(500).json({error: error});
        }
    }

    async changeActivo(req: Request, res: Response){
        const repo      = new EspacioDBRepository();
        const serviceE = new EspacioService(repo);
        const status = req.body.status;
        const id = req.body.id;
        try {
            const result = await serviceE.changeActivo(id,status);
            res.status(200).json({payload: result})
        } catch (error) {
            res.status(500).json({error: error});
        }
    }

    async deleteEspacio(req: Request, res: Response){
        let id = req.params.id;
        const repo      = new EspacioDBRepository();
        const serviceE = new EspacioService(repo);
        try{
            const result = await serviceE.deleteEspacio(parseInt(id));
            res.status(200).json({payload: result})
        }catch (err){
            res.status(500).json({error: err})
        }
    }


}