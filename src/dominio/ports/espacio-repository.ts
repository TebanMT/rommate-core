import {Espacio } from "../entities/espacio-entity";

export interface EspacioRepository{

    getMinumunDataEspacios(lugar?:string, tipo?:string, fecha?:string): Promise<Espacio[] | null>;
    getMinumunDataEspaciosByAnfitrion(id_anfitrion:string): Promise<Espacio[] | null>;
    getEspacio(id: number): Promise<Espacio | null>
    createEspacio(espacio: Espacio): Promise<Espacio>;
    getCountCities(): Promise<any[]>;
    deleteEspacio(id_espacio:number):Promise<boolean>;
    changeActivo(id_espacio:number,status:boolean): Promise<boolean>;
    //searchEspacios(): Promise<Espacio[] | null>;
}