import { Request, Response } from "express";
import Usuario from "../../../dominio/entities/user-entity";
import { UserDBRepository } from "../../../infrestructura/db/repositories/user-db-repository";
import { JwtTokenRepository } from "../../../infrestructura/security/jwt-token-repository";
import { UserService } from "../../../servicios";
import { UsuarioDTO } from "../dtos/usuario-dto";
import { Mappers } from "../mappers";

export class UserController {

    async find(req:Request, res:Response){
        const id = parseInt(req.params.id);
        const repo = new UserDBRepository();
        const userService = new UserService(repo);
        try {
            const user = await userService.find(id);
            res.status(200).json({payload: user});
        } catch (error) {
            console.log(error);
            res.status(500).json({error: error});
        }
    }

    async registerUser(req:Request, res:Response){
        const repo = new UserDBRepository();
        const security = new JwtTokenRepository();
        const userService = new UserService(repo, security);
        const body: Usuario = req.body as Usuario;
        try {
            const user:UsuarioDTO = Mappers.userToEntitieDomain(await userService.registerUser(body));
            res.status(201).json({payload: user});
        } catch (err) {
            res.status(500).json({error: err});
        }
    }

    async login(req: Request, res: Response){
        const repo = new UserDBRepository();
        const security = new JwtTokenRepository();
        const userService = new UserService(repo, security);
        const body: Usuario = req.body as Usuario;
        try {
            var respose: UsuarioDTO | {};
            var status: number;
            const thereIsUser = await userService.login(body);
            if (thereIsUser) {
                respose = Mappers.userToEntitieDomain(thereIsUser);
                status = 200;
            } else {
                respose = {message:"El Usuario no se encuentra registrado"};
                status = 204;
            }
            res.status(status).json({payload: respose});
        } catch (err) {
            console.log(err)
            res.status(500).json({error: err});
        }
    }
}