import Usuario from "../../../dominio/entities/user-entity";
import { UserRepository } from "../../../dominio/ports";
import { Mappers } from "../mappers";
import { User } from "../orm";
const { Op } = require("sequelize");

export class UserDBRepository implements UserRepository{

    async find(id: number): Promise<Usuario | null> {
        const userModel = await User.findOne({
            where: {
                id: id,
            }
        });
        const u: Usuario | null = userModel ? Mappers.userToEntitieDomain(userModel): null;
        return u;
    }

    async createUser(user: Usuario): Promise<Usuario>{
        const userModel = await User.create(user);
        const u: Usuario = Mappers.userToEntitieDomain(userModel);
        return u;
    }

    async getUserByPhone(lada: string, number: string): Promise<Usuario | null> {
        var u: Usuario | null;
        const userModel = await User.findOne({
            where: {
                numero_cel: number,
                lada_int : lada
            }
        });
        if (userModel) {
             u = Mappers.userToEntitieDomain(userModel);
        }else{
            u = null;
        }
        return u;
    }

    async existUser(user: Usuario): Promise<boolean> {
        var u: boolean;
        const userModel = await User.findOne({
            where:{
                [Op.or]: [
                    {
                        [Op.and] : [
                            {
                                numero_cel: user.numero_cel
                            },
                            {
                                lada_int : user.lada_int
                            }
                        ]
                    },
                    {
                        email : user.email || ''
                    }
                ]
            }
        });
        u = userModel ? true : false;
       return u;
    }

    async updateAnfirion(id_user: number, status: boolean): Promise<boolean> {
        const userReturn = await User.update(
            {anfitrion : status},
            {where: {id : id_user}}
        );
        return userReturn ? true : false;;
    }

    async getFcmToken(id_user: number): Promise<string | undefined> {
        const userModel = await User.findOne({
            where: {
                id: id_user,
            },
            attributes: ['token_fcm']
        });
        return userModel?.token_fcm;
    }
}