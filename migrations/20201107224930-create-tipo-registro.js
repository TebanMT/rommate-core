'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.createTable('tipo_registro', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        tipo: {
          allowNull: false,
          type: Sequelize.STRING
        },
        descripcion: {
          allowNull: false,
          type: Sequelize.TEXT
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE
        }
    }),
    await queryInterface.sequelize.query('CREATE TRIGGER tipo_registro_created_trigger_text'+
      ' BEFORE INSERT ON tipo_registro FOR EACH ROW SET NEW.createdAt = NOW();'),
    await queryInterface.sequelize.query('CREATE TRIGGER tipo_registro_updated_trigger_text'+
      ' BEFORE UPDATE ON tipo_registro FOR EACH ROW SET NEW.updatedAt = NOW();')
  ]
  },
  down: async (queryInterface, Sequelize) => {
    return[
      await queryInterface.sequelize.query('DROP TRIGGER tipo_registro_created_trigger_text'),
      await queryInterface.sequelize.query('DROP TRIGGER tipo_registro_updated_trigger_text'),
      await queryInterface.dropTable('tipo_registro')
    ]
  }
};