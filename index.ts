// Export the application
const containerREST   = require("./src/interfaces/api rest/container");
//const containerSocket = require("./src/interfaces/websocket/container");

import { dbConfig } from "./src/infrestructura/db/orm";

const applicationREST   = containerREST.resolve("app");
//const applicationSocket = containerSocket.resolve("app");


// RUN API REST
applicationREST.start();


// RUN MYSQL DB SERVER
dbConfig
.authenticate()
.then(() => console.log("connected to db"))
.catch((err) => {
    console.log(err)
    throw "error";
});

// RUN WEBSOCKET

//applicationSocket.start();