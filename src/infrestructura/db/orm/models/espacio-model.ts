import * as Sequelize from 'sequelize'
import { BuildOptions, Model, DataTypes, DateOnlyDataType } from 'sequelize';

export interface EspacioAttributes {
  id: number;
  titulo: string;
  precio_renta: number;
  cantidad_personas: number;
  descripcion_espacio?: string;
  descripcion_gral?: string;
  diponible        : boolean;
  fecha_disponibilidad?: string;
  id_direccion: number;
  id_tipo_espacio: number;
  id_usuario: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface EspacioModel extends Model<EspacioAttributes>, EspacioAttributes {};
export class Espacio extends Model<EspacioModel, EspacioAttributes> {
};

export type EspacioStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): EspacioModel;
};

export const EspacioFactory = (sequelize: Sequelize.Sequelize): EspacioStatic => {
  return <EspacioStatic>sequelize.define('espacio',{
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    titulo: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    precio_renta: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    cantidad_personas: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    descripcion_espacio: {
      type: DataTypes.TEXT
    },
    descripcion_gral: {
      type: DataTypes.TEXT
    },
    diponible: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    fecha_disponibilidad: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    id_direccion: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    id_tipo_espacio: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    id_usuario: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
  });
}